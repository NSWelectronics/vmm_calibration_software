# vmm_calibration_software

# Contents
* [Introduction](#introduction)
* [Supported Calibration Analysies](#supported-calibration-analyses)
  * [BaselinesXADC](#baselinesxadc)
  * [ChannelTrimsXADC](#channeltrimsxadc)
  * [PulserDACXADC](#pulserdacxadc)
  * [ThresholdDACXADC](#thresholddacxadc)
  * [GainsPedestal](#gainspedestal)
  * [TimingCKBCDelay](#timingckbcdelay)
* [Installation](#installation)
* [Running](#running)
* [Questions](#questions-comments)

# Introduction

This is a package that contains the necessary software to perform calibration analyses on the output of the calibration runs from [vmm_readout_software/][1].

Currently there are no releases, so grab the **master** branch.

# Supported Calibration Analyses

The supported calibration analyses are defined by their associated [vmmcalib::CalibrationType][2] and each has an associated set of so-called "loopers" and "holders"
which perform the analysis and hold the calibration data for storage, etc... 

Each of the analysis "loopers" outputs several summary and diagnostic plots of the related calibration data and performance as well as text files which hold all of
relevant data for performing further data analysis. Currently, only in the case of the **vmmcalib::CalibrationType::ChannelTrimsXADC** calibration there is also an output
*XML* file which may be provided to the [verso][1] software to load in the VMM channel threshold trimmer calibration for use when configuring the associated front end electronics.

## BaselinesXADC

**xADC based calibration procedure**

This calibration samples each channels' input baseline analog level. From this measurement we get the overall **input baseline** to each channel as well as a measurement of the
**noise** of the channel input.

**Output data:**

    1) baseline for each (board,chip,channel) tuple
    2) noise measurement for each (board,chip,channel) tuple

## ChannelTrimsXADC

**xADC based calibration procedure**

This calibration samples each channel's threshold, for each of the channel threshold trimmer steps taken in the calibration loop. From this measurement, we get
the overall channel threshold trimmer **range** and the determination of the channel threshold trimmer **set** that equalizes all of the provided channels' thresholds
(max voting).

**Output data:**

    1) channel threshold range for each (board,chip,channel) tuple
    2) channel trimmer setting that equalizes all channels to one another for each (board,chip,channel) tuple
    
## PulserDACXADC

**xADC based calibration procedure**

This calibration samples the analog values of the global VMM pulser DAC, which is used for injecting the charge to each of the VMM's test pulse capacitors. From
this measurement we can get a conversion of the VMM configured DAC to mV which allows us to get an absolute measure of the injected charge to the capacitors.

**Output data:**

    1) Parameters (slope and constant) to convert pulser DAC into mV for each (board,chip) tuple
    
## ThresholdDACXADC
    
**xADC based calibration procedure**

This calibration samples the analog values of the global VMM threshold DAC, which is used for setting the global VMM channel thresholds. From this measurement,
we can get a conversion of the VMM configured threshol DAC to mV, which allows us to get an absolute measure of the VMM threshold setting.

**Output data:**

    1) Parameters (slope and constant) to convert threshold DAC into mV for each (board,chip) tuple
    
## GainsPedestal

**Pulser based calibration procedure**

This calibration measures the relationship between each channel's PDO output versus the pulser DAC setting to get a measurement of the
 **channel-by-channel gain variation** to correct off-line analysis. A **pedestal** measurement is also made using the y-intercept of the straight-line fit
 of the gain curve.

**Output data:**

    1) gain parameter for each (board,chip,channel) tuple
    2) PDO pedestal for each (board,chip,channel) tuple
    
## TimingCKBCDelay

**Pulser based calibration procedure**

This calibration measures the TDO conversion constants as well as two methods of measuring the TDO pedestal. It relies on the **"Fixed Window"**
acquisition mode of [verso][1], where the TAC ramp stops with a fixed latency and we step through this latency to get the relationship of TDO vs latency (time).

**Output data:**

    1) TDO conversion constant (counts to ns) for each (board,chip,channel) tuple
    2) TDO pedestal measured from the straight-line fit of the TDO-versus-BC latency curve (in the region after the peak is found, i.e. TAC ramp started) for each (board,chip,channel) tuple
    3) TDO pedestal measured from the constant fit of the TDO-versus-BC latency curve (in the region before the peak is found, i.e. TAC ramp not yet started) for each (board,chip,channel) tuple
    
# Installation

Check out this package.

This package requires [*CMake*][3]. *CMake* is now the standard for all *ATLAS* software, so familiarity is assumed. *CMake* version 3.8 or higher is required.

Once the package is checked out from GitLab in the usual way follow the usual *CMake* setup steps:

```
$ cd vmm_calibration_software/
$ source build_stl.sh
$ mkdir build; cd build; cmake ..;
$ make
```

If you use **ROOT6** you can provide the option ```--root6``` to the ```build_stl.sh``` command,

```
$ source build_stl.sh --root6
```

Try running *build_stl.sh* with the ```--help``` option for further information. The ```build_stl.sh``` script only needs to be run once per checkout of
the software, it builds a library for handling *STL* collections within ROOT and puts an associated binary under the ```dict/``` directory. If the ```*.so```
file is not located under ```dict/``` during the build steps listed above, the compilation will fail.

The *Boost* headers for *boost/property_tree* are required. If the compilation above did not work, please find where your *Boost* installation is,
and **pre-pend**  the ```<path-to-your-boost>/boost/include``` directory to your ```CMAKE_PREFIX_PATH```:

```
export CMAKE_PREFIX_PATH=<path-to-your-boost>/boost/include:${CMAKE_PREFIX_PATH}
```

Doing so will allow the *CMake* *find_package(...)* macro to successfully locate your *Boost* installation.

# Running

After succesful compilation, the ```vmmcalib``` executable will be located under *vmm_calibration_software/build*. To see a full list of options run:

```
$ ./vmmcalib -h
```

The minimal required options are the input (```-i|--input```) calibration n-tuple and the calibration type (```-t|--type```). In the help menu,
the allowed calibration types are listed.

The ```-o|--output``` option specifies an output directory (one will be made if it is not found) where the calibration analysis will output
data and plots. By default, the output directory (if the ```-o|--output``` option is not specified) all of the data and plots will be placed
in directories called ```data/``` and ```plots/``` in the directory where ```vmmcalib``` is run. By specifying an output directory, for example
```--output <path-to-here>/test_beam_calib```, upon succesful running, the following directories will appear in the directory ```<path-to-here>``` directory:

```
<path-to-here>/test_beam_calib/
<path-to-here>/test_beam_calib/data/
<path-to-here>/test_beam_calib/plots/
```

By providing the ```-n``` or ```--name``` option, you provide a descriptive name to the data. This name will be added as a suffix to all output data and 
plot files, as well as to the names of the output directories. For example, by providing the output parameter ```-o <path-to-here>/test_beam_calib``` and name
parameter ```-n timing_board_02```, the following directories will appear in the directory ```<path-to-here>```:

```
<path-to-here>/test_beam_calib/
<path-to-here>/test_beam_calib/data_timing_board_02/
<path-to-here>/test_beam_calib/plots_timing_board_02/
```

An assumption that is made in each of the so-called "loopers" is that only one set of "run properties" exists. For example, if you did the ```ChannelTrimsXADC```
calibration loop over multiple threshold DAC values, then you will have VMM channel trim loops repeated for each of those threshold DAC values. If your
file does have multiple, e.g. DAC, values, you must specify the one you wish to perform the calibration over by providing the ```--select-*``` options. At
the current time there are the:

```
--select-pulser
--select-threshold
--select-gain
--select-tac
--select-peak-time
```

options that may be provided to ```vmmcalib```. If you only wish to calibrate a single board, chip, or channel (or combination) you can provide the

```
--board
--chip
--channel
```

options to ```vmmcalib```. For example, let's say your ```ChannelTrimsXADC``` calibration n-tuple was done for threshold DAC values 230, 300, and 350
over a single board with 8 VMM's on it. You can do:

```
$ ./vmmcalib -i <path-to-file>.root -t ChannelTrimsXADC --chip 0 --select-threshold 300
```

to run the calibration analysis over only the data from VMM 0 with threshold DAC setting 300. If you do not provide the ```--board```, ```--chip```, or ```--channel```
options, the calibration loop will process all boards, chips, and channels found in the calibration n-tuple.


# Questions Comments
For questions, comments, or aob, contact: **Daniel Antrim**: <daniel.joseph.antrim@cern.ch>



[1]: https://gitlab.cern.ch/NSWelectronics/vmm_readout_software
[2]: https://gitlab.cern.ch/NSWelectronics/vmm_calibration_software/blob/master/include/calib_base/calibration_type.h#L8
[3]: https://cmake.org/download/