#ifndef VMM_CALIB_HOLDER_BASELINES_NOISE_H
#define VMM_CALIB_HOLDER_BASELINES_NOISE_H

namespace vmmcalib {

    class HolderBaselinesNoise
    {
        public :

            HolderBaselinesNoise() { clear(); }
            virtual ~HolderBaselinesNoise(){};

            // coordinates
            int board;
            int chip;
            int channel;

            // mean baseline
            double baseline;

            // error (noise) on baseline
            double baseline_error;

            void clear() {
                board = 0;
                chip = 0;
                channel = 0;
                baseline = 0.0;
                baseline_error = 0.0;
            }

    }; // class

}; // namespace

#endif
