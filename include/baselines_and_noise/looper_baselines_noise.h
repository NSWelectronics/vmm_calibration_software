#ifndef VMM_CALIB_BASELINES_NOISE_H
#define VMM_CALIB_BASELINES_NOISE_H

///////////////////////////////////////////////////////////////////////
//
// looper_baselines_noise
//
// looper class for performing the xADC based VMM channel baseline
// and noise measurement
//
// daniel.joseph.antrim@cern.ch
// October 2017
//
///////////////////////////////////////////////////////////////////////

//std/stl

//vmmcalib
#include "calib_base.h"
#include "holder_baselines_noise.h"

namespace vmmcalib {

    class LooperBaselinesNoise : public CalibBase
    {
        public :
            LooperBaselinesNoise();
            virtual ~LooperBaselinesNoise(){};

            void run_calibration();

            void get_baselines_and_noise();
            void get_baseline_and_noise(int board, int chip, int channel);

            void get_summaries();
            void get_summary(int board, int chip);
            void get_noise_summary(int board, int chip);
            void get_noise_summary_board(int board);

            void write_txt_file();

            float x_to_mv(float counts);
            float x_to_mv(int counts);

            std::map<BoardChipChannelTuple, HolderBaselinesNoise> holder;
            std::map<BoardChipPair, std::vector<int> > bad_channel_holder;
            std::map<BoardChipPair, std::pair<float, float> > summary_holder;

    }; // class

}; // namespace

#endif
