#ifndef VMM_CALIB_CALIB_BASE_H
#define VMM_CALIB_CALIB_BASE_H

//////////////////////////////////////////////////////////////////
//
// calib_base
//
// base class for all calibration loopers
// purpose: holds the input file, global options, output dir, etc..
//
// daniel.joseph.antrim@cern.ch
// September 2017
//
//////////////////////////////////////////////////////////////////

//std/stl
#include <string>
#include <map>
#include <tuple>
#include <vector>
#include <cstdint> // uintX_t

//vmmcalib
#include "calibration_type.h"
#include "calib_event.h"

//ROOT
class TFile;
class TChain;
class TBranch;

namespace vmmcalib {

    typedef std::pair<int, int> BoardChipPair;
    typedef std::tuple<int, int, int> BoardChipChannelTuple;

    class CalibBase
    {
        public :
            CalibBase();
            virtual ~CalibBase(){};

            virtual bool load_baselines(std::string /*filename*/) { return true; };
            virtual bool load_threshold_calibration(std::string /*filename*/) { return true; }

            void set_dbg(int d) { m_dbg = d; }
            int dbg() { return m_dbg; }

            void set_name(std::string name) { m_name = name; }
            std::string name() { return m_name; }

            void set_calibration_type(CalibrationType type) { m_type = type; }
            CalibrationType type() { return m_type; }

            void set_select_board(int board) { m_select_board = board; }
            int select_board() { return m_select_board; }

            void set_select_chip(int chip) { m_select_chip = chip; }
            int select_chip() { return m_select_chip; }

            void set_select_channel(int channel) { m_select_channel = channel; }
            int select_channel() { return m_select_channel; }

            void set_n_chan_override(int n) { m_n_chan_override = n; }
            int n_chan_override() { return m_n_chan_override; }

            void set_select_gain(float gain) { m_select_gain = gain; }
            float select_gain() { return m_select_gain; }

            void set_select_peak_time(int pt) { m_select_peak_time = pt; }
            int select_peak_time() { return m_select_peak_time; }

            void set_select_pulser(int pulser) { m_select_pulser = pulser; }
            int select_pulser() { return m_select_pulser; }

            void set_select_threshold(int threshold) { m_select_threshold = threshold; }
            int select_threshold() { return m_select_threshold; }

            void set_select_tac(int tac) { m_select_tac = tac; }
            int select_tac() { return m_select_tac; }

            void set_output_dir(std::string outputdir);
            std::string output_dir() { return m_output_dir; }
            std::string plot_dir() { return m_plot_dir; }
            std::string data_dir() { return m_data_dir; }

            void set_all_plots(bool doit) { m_do_all_plots = doit; }
            bool all_plots() { return m_do_all_plots; }

            bool load_file(std::string filename);
            bool connect_tree();
            bool connect_pulser_tree();


            TFile* file() { return m_tfile; }
            TChain* chain() { return m_chain; }

            void clear();

            virtual void run_calibration();
            void get_events();
            void get_pulser_events();

            void load_board(int board);
            bool board_loaded(int board);

            void load_chip(int chip);
            bool chip_loaded(int chip);

            void load_channel(int channel);
            bool channel_loaded(int channel);

            void load_trim(int trim);
            bool trim_loaded(int trim);

            void load_dac(int dac);
            bool dac_loaded(int dac);

            int m_select_board;
            int m_select_chip;
            int m_select_channel;

            int m_n_chan_override;

            float m_select_gain;
            int m_select_peak_time;
            int m_select_pulser;
            int m_select_threshold;
            int m_select_tac;

        private :
            int m_dbg;
            CalibrationType m_type;
            double m_events_gathered;


            TFile* m_tfile;
            TChain* m_chain;

            std::string m_name;
            std::string m_output_dir;
            std::string m_plot_dir;
            std::string m_data_dir;

            bool m_do_all_plots;

            // TTree
            TBranch* br_board;
            TBranch* br_chip;
            TBranch* br_channel;

            TBranch* br_samples; // xadc
            TBranch* br_channel_trim; // xadc

            TBranch* br_pulser_dac; // properties
            TBranch* br_threshold_dac; // properties
            TBranch* br_gain; // properties
            TBranch* br_peak_time; // properties
            TBranch* br_tac_slope; // properties

            // PULSER TTree
            TBranch* br_pulser_board;
            TBranch* br_pulser_chips;
            TBranch* br_pulser_trigger_count;
            TBranch* br_pulser_bcids;
            TBranch* br_pulser_grays;
            TBranch* br_pulser_boards;
            TBranch* br_pulser_channels;
            TBranch* br_pulser_neighbor_enabled;
            TBranch* br_pulser_tac_slope;
            TBranch* br_pulser_peak_time;
            TBranch* br_pulser_pulser_dac;
            TBranch* br_pulser_threshold_dac;
            TBranch* br_pulser_pdo;
            TBranch* br_pulser_tdo;
            TBranch* br_pulser_pass_threshold;
            TBranch* br_pulser_is_neighbor;
            TBranch* br_pulser_tp_skew;
            TBranch* br_pulser_bc_latency;
            

        protected :

            // (selected) BCC loaded from file
            std::vector<int> loaded_boards;
            std::vector<int> loaded_chips;
            std::vector<int> loaded_channels;

            bool m_subtract_baselines;
            bool m_threshold_calib_loaded;

            // loaded events
            std::map<BoardChipChannelTuple, std::vector<CalibEvent> > events;
            std::map<BoardChipChannelTuple, std::map<int, int> > channel_counts;

            // TTree
            uint32_t m_board;
            uint32_t m_chip;
            uint32_t m_channel;

            uint32_t m_sample; // xadc
            int32_t m_channel_trim; // xadc
            std::vector<int> loaded_trims; // xadc
            std::vector<int> loaded_dacs; // xadc

            int32_t m_pulser_dac; // properties
            int32_t m_threshold_dac; // properties

            double m_gain;
            int32_t m_peak_time;
            int32_t m_tac_slope;

            // Pulser TTree

            uint32_t m_pulser_board;
            float m_pulser_pulser_dac;
            float m_pulser_threshold_dac;
            float m_pulser_tac_slope;
            float m_pulser_peak_time;
            float m_pulser_tp_skew;
            float m_pulser_bc_latency;
            std::vector<int>* m_pulser_chips;
            std::vector<int>* m_pulser_boards;
            std::vector<int>* m_pulser_trigger_count;
            std::vector< std::vector<int> >* m_pulser_bcids;
            std::vector< std::vector<int> >* m_pulser_grays;
            std::vector< std::vector<int> >* m_pulser_channels;
            std::vector< std::vector<int> >* m_pulser_pdo;
            std::vector< std::vector<int> >* m_pulser_tdo;
            std::vector< std::vector<int> >* m_pulser_is_neighbor;
            std::vector< std::vector<int> >* m_pulser_pass_threshold;
            

    };


}; // namespace

#endif
