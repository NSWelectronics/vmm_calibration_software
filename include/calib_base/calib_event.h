#ifndef VMM_CALIB_CALIB_EVENT_H
#define VMM_CALIB_CALIB_EVENT_H

///////////////////////////////////////////////////////////
//
// calib_event
//
// holder class for each event's data fields
// expose everything, for the lolz
//
// daniel.joseph.antrim@cern.ch
// September 2017
//
///////////////////////////////////////////////////////////

//std/stl
#include <cstdlib>

namespace vmmcalib {

    class CalibEvent
    {

        public :
            CalibEvent(){ clear(); };
            virtual ~CalibEvent(){};

            // common
            uint32_t board;
            uint32_t chip;
            uint32_t channel;

            // xadc
            uint32_t sample;
            uint32_t channel_trim;

            // properties
            uint32_t pulser_dac;
            uint32_t threshold_dac;
            float gain;
            uint32_t tac_slope;
            uint32_t peak_time;


            void clear() {
                board = 0;
                chip = 0;
                channel = 0;
                sample = 0;
                channel_trim = 0;
                pulser_dac = 0;
                threshold_dac = 0;
                gain = 0.0;
                tac_slope = 0;
                peak_time = 0;
            }

            

    }; // class

}; // namespace

#endif
