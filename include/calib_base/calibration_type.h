#ifndef VMM_CALIB_CALIBRATION_TYPE_H
#define VMM_CALIB_CALIBRATION_TYPE_H

#include <string>

namespace vmmcalib {

    enum CalibrationType {
        BaselinesXADC=0,
        ChannelTrimsXADC,
        PulserDACXADC,
        ThresholdDACXADC,
        GainsPedestal,
        TimingCKBCDelay,
        Invalid
    };

    //human readable
    std::string CalibrationType2Str(const CalibrationType& c);
    CalibrationType CalibrationTypeFromStr(const std::string& c);
    bool is_valid_type(std::string name);
    std::string valid_types();
    bool is_xadc(const CalibrationType& c);


}; // namespace

#endif
