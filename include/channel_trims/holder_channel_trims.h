#ifndef VMM_CALIB_HOLDER_CHANNEL_TRIMS_H
#define VMM_CALIB_HOLDER_CHANNEL_TRIMS_H

namespace vmmcalib {

    class HolderChannelTrims
    {

        public :
            HolderChannelTrims() {clear(); }
            virtual ~HolderChannelTrims(){};

            // coordinates
            int board;
            int chip;
            int channel;

            // for each trim, store the threshold and error
            std::map<int, std::pair<float, float> > trim_map;

            // threshold range (in mV)
            float threshold_range;

            // the trim value associated with the minimum possible threshold
            // (this defines the range in trim steps)
            int minimum_trim;

            // flag that says whether this channel has all 32-trim steps
            bool full_trim;

            void clear() {
                board = 0;
                chip = 0;
                channel = 0;

                trim_map.clear();

                threshold_range = 0.0;
                minimum_trim = 0;
                full_trim = false;
            }


    }; // class
}; // namespace

#endif
