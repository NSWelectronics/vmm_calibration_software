#ifndef VMM_CALIB_LOOPER_CHANNEL_TRIMS_H
#define VMM_CALIB_LOOPER_CHANNEL_TRIMS_H

//////////////////////////////////////////////////////////////////////
//
// looper_channel_trims
//
// looper class for performing the xADC based VMM channel threshold 
// trimmer calibration
//
// daniel.joseph.antrim@cern.ch
// September 2017
//
//////////////////////////////////////////////////////////////////////

//std/stl

//vmmcalib
#include "calib_base.h"
#include "holder_channel_trims.h"

namespace vmmcalib {

    class LooperChannelTrims : public CalibBase
    {
        public :
            LooperChannelTrims();
            virtual ~LooperChannelTrims(){};

            bool load_baselines(std::string filename);
            bool load_threshold_calibration(std::string filename);
            float calculate_threshold_dac(int board, int chip);

            void run_calibration();

            void get_channel_thresholds_and_ranges();
            void get_threshold_per_trim(int board, int chip, int channel);
            void get_threshold_range(int board, int chip, int channel);

            void get_best_trims();
            void get_best_trims_per_chip(int board, int chip);
            void get_best_trims_per_board(int board);
            void get_best_trims_global();

            void make_summary_plots();
            void range_per_channel_plot(int board, int chip);
            void best_summary_plot_chip(int board, int chip);

            void write_txt_file();
            void write_txt_file(std::string type);

            void write_xml_file();
            void write_xml_file(std::string type); 

            float x_to_mv(float counts);
            float x_to_mv(int counts);

            // holder for data
            std::map<BoardChipChannelTuple, HolderChannelTrims> holder;

            std::map<BoardChipChannelTuple, float> baselines_holder; // float is xADC counts, not mV!!

            std::map<BoardChipPair, std::pair<float, float> > threshold_calib_holder;

            // holder for "best trims" per chip
            // [bcp : [channel : trim] ]
            std::map<BoardChipPair, std::vector< std::pair<int, int> > > best_trims_chip;

            // holder for "best trims" per board
            std::map<BoardChipPair, std::vector< std::pair<int, int> > > best_trims_board;

            // holder for "best trims" global
            std::map<BoardChipPair, std::vector< std::pair<int, int> > > best_trims_global;


            int m_max_selected_threshold_cts;
            float m_max_selected_threshold;

        private :

    }; // class


}; // namespace

#endif
