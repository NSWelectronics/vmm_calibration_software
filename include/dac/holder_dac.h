#ifndef VMM_CALIB_HOLDER_DAC_H
#define VMM_CALIB_HOLDER_DAC_H

namespace vmmcalib {

    class HolderDAC
    {
        public :

            HolderDAC() { clear(); }
            virtual ~HolderDAC(){};

            // coordinates
            int board;
            int chip;

            float dac_slope; // VMM XADC counts / DAC counts
            float dac_slope_error;

            float dac_constant;
            float dac_constant_error;

            void clear() {
                board = 0;
                chip = 0;
                dac_slope = 0.0; // mV/cts
                dac_slope_error = 0.0; // mV/cts
                dac_constant = 0.0;
                dac_constant_error = 0.0;
            }

    }; // class

}; // namespace



#endif
