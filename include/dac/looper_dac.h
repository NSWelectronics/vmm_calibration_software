#ifndef VMM_CALIB_DAC_H
#define VMM_CALIB_DAC_H

///////////////////////////////////////////////////////////////////////
//
// looper_dac
//
// looper class for performing the xADC based pulser and threshold
// DAC calibration
//
// daniel.joseph.antrim@cern.ch
// October 2017
//
///////////////////////////////////////////////////////////////////////

//std/stl

//vmmcalib
#include "calib_base.h"
#include "holder_dac.h"

namespace vmmcalib {

    class LooperDAC : public CalibBase
    {
        public :
            LooperDAC();
            virtual ~LooperDAC(){};

            void run_calibration();
            void load_params();
            void write_txt_file();

            std::map<BoardChipPair, HolderDAC> holder;

    }; // class

}; // namespace

#endif
