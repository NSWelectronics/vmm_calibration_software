#ifndef VMM_CALIB_HOLDER_GAINS_PEDESTAL_H
#define VMM_CALIB_HOLDER_GAINS_PEDESTAL_H

namespace vmmcalib {

    class HolderGainsPedestal
    {
        public :
            HolderGainsPedestal() { clear(); }
            virtual ~HolderGainsPedestal(){};

            // coordinates
            int board;
            int chip;
            int channel;

            float gain; // [VMM PDO counts/DAC counts]
            float gain_error; // [VMM PDO counts/DAC counts]

            float pedestal; // [VMM PDO counts]
            float pedestal_error; // [VMM PDO count]

            void clear() {
                board = 0;
                chip = 0;
                channel = 0;
                gain = 0.0;
                gain_error = 0.0;
                pedestal = 0.0;
                pedestal_error = 0.0;
            }

    }; // class

}; // namespace

#endif
