#ifndef VMM_CALIB_LOOPER_GAINS_PEDESTAL_H
#define VMM_CALIB_LOOPER_GAINS_PEDESTAL_H


//////////////////////////////////////////////////////////////////////
//
// looper_gains_pedestal
//
// looper class for performing pulser based VMM channel gain and
// PDO pedestal calibration
//
// daniel.joseph.antrim@cern.ch
// October 2017
//
//////////////////////////////////////////////////////////////////////

//std/stl
#include "calib_base.h"
#include "holder_gains_pedestal.h"

namespace vmmcalib {

    class LooperGainsPedestal : public CalibBase
    {
        public :
            LooperGainsPedestal();
            virtual ~LooperGainsPedestal(){};

            void run_calibration();
            void load_params();
            void get_gain_and_pedestal(int board, int chip, int channel);
            void write_txt_file();

        private :
            std::map<BoardChipChannelTuple, HolderGainsPedestal> holder;

    };


}; // vmmcalib


#endif
