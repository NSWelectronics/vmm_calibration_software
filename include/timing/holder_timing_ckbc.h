#ifndef VMM_CALIB_HOLDER_CKBC_H
#define VMM_CALIB_HOLDER_CKBC_H

namespace vmmcalib {

    class HolderCKBC
    {
        public :
            HolderCKBC() { clear(); }
            virtual ~HolderCKBC(){};

            // coordinates
            int board;
            int chip;
            int channel;

            float slope; // [cts/ns]
            float slope_error; // [cts/ns]

            float pedestal_fit; // [cts]
            float pedestal_fit_error; // [cts]

            float pedestal_flat; // [cts]
            float pedestal_flat_error; // [cts]

            void clear() {
                board = 0;
                chip = 0;
                channel = 0;

                slope = 0.0;
                slope_error = 0.0;
                pedestal_fit = 0.0;
                pedestal_fit_error = 0.0;
                pedestal_flat = 0.0;
                pedestal_flat_error = 0.0;
            }

    }; // class

}; // namespace

#endif
