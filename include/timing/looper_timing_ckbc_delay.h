#ifndef VMM_CALIB_LOOPER_TIMING_CKBC_DELAY_H
#define VMM_CALIB_LOOPER_TIMING_CKBC_DELAY_H

//////////////////////////////////////////////////////////////////////
//
// looper_timing_ckbc_delay
//
// looper class for performing VMM timing calibration using CKBC
// delay method
//
// daniel.joseph.antrim@cern.ch
// October 2017
//
//////////////////////////////////////////////////////////////////////

//std/stl
#include <vector>

//vmmcalib
#include "calib_base.h"
#include "holder_timing_ckbc.h"

namespace vmmcalib {

    class LooperTimingCKBC : public CalibBase
    {
        public :
            LooperTimingCKBC();
            virtual ~LooperTimingCKBC(){};

            void run_calibration();
            void load_params();
            void get_constants_and_pedestal(int board, int chip, int channel);
            void summary_plots(int board, int chip);
    
            void write_txt_file();

        private :
            std::vector<float> loaded_latencies;
            std::vector<int> loaded_tacs;
            std::vector<int> loaded_pts;

            std::map<BoardChipChannelTuple, HolderCKBC> holder;
    };

}; // namespace

#endif
