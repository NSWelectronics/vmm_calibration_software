#include "looper_baselines_noise.h"

using namespace vmmcalib;

//std/stl
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
using namespace std;

//ROOT
#include "TStyle.h"
#include "TLegend.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TLatex.h"
#include "TLine.h"
#include "TGaxis.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TH2.h"


/////////////////////////////////////////////////////////////////////////////
LooperBaselinesNoise::LooperBaselinesNoise()
{
    cout << " >>> LooperBaselinesNoise" << endl;
}
/////////////////////////////////////////////////////////////////////////////
void LooperBaselinesNoise::run_calibration()
{
    get_events(); // c.f. vmm_calibration_software/calib_base.h

    // get the baselines and noise for each of the loaded boards, chips, and channels
    get_baselines_and_noise();

    // get summary information for each (board, chip) pair
    get_summaries();

    write_txt_file();
}
/////////////////////////////////////////////////////////////////////////////
void LooperBaselinesNoise::get_baselines_and_noise()
{
    for(auto board : loaded_boards) {
        for(auto chip : loaded_chips) {
            for(auto channel : loaded_channels) {
                get_baseline_and_noise(board, chip, channel);
            } // channel
        } // chip
    } // board

}
/////////////////////////////////////////////////////////////////////////////
void LooperBaselinesNoise::get_baseline_and_noise(int board, int chip, int channel)
{
    gStyle->SetOptFit(111);

    BoardChipChannelTuple tuple(board, chip, channel);
    vector<CalibEvent> calib_samples = events[tuple];

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip << "_ch" << channel;

    stringstream n;
    n << "c_baselines" << coordstr.str();

    TCanvas* c = new TCanvas(n.str().c_str(), "", 800, 600); 
    c->SetGrid(1,1);
    c->cd();

    n.str("");
    n << "h_baseline_xadc" << coordstr.str(); 

    TH1F* h = new TH1F(n.str().c_str(), "", 4096, 0, 4096);
    h->SetLineColor(kBlack);
    h->GetXaxis()->SetTitle("xADC samples [counts]");
    h->GetXaxis()->SetTitleFont(42);
    h->GetXaxis()->SetLabelFont(42);
    h->GetYaxis()->SetTitle("Entries");
    h->GetYaxis()->SetTitleFont(42);
    h->GetYaxis()->SetLabelFont(42);

    double max_x = -1;
    double min_x = 5000;

    for(auto s : calib_samples) {
        h->Fill(s.sample);
        if(s.sample > max_x) max_x = s.sample;
        if(s.sample < min_x) min_x = s.sample;
    } // s

    n.str("");
    n << "f_gaus";
    TF1* f = new TF1(n.str().c_str(), "gaus", min_x, max_x);
    f->SetParNames("Constant", "Mean", "Sigma");
    h->Fit(f, "QR");

    float mean = f->GetParameter(1);
    float sigma = f->GetParameter(2);

    // load
    HolderBaselinesNoise hbx;
    hbx.board = board;
    hbx.chip = chip;
    hbx.channel = channel;
    hbx.baseline = mean;
    hbx.baseline_error = sigma;
    holder[tuple] = hbx;

    double maxy = h->GetMaximum();
    maxy = 1.2 * maxy;

    max_x = (max_x + 10);
    min_x = (min_x - 10);

    h->GetXaxis()->SetRangeUser(min_x, max_x);
    h->SetMaximum(maxy);

    h->Draw("hist");
    c->Update();
    f->Draw("same");
    c->Update();

    // save
    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "calib_baseline_samples" << coordstr.str();
    if(name()!="") n << "_" << name();  
    n << ".pdf";
    c->SaveAs(n.str().c_str());
    delete c;
   

}
/////////////////////////////////////////////////////////////////////////////
float LooperBaselinesNoise::x_to_mv(float counts)
{
    return (( counts / pow(2,12)) * 1000. );
}
float LooperBaselinesNoise::x_to_mv(int counts)
{
    return (( (1.0 * counts) / pow(2,12)) * 1000. );
}
/////////////////////////////////////////////////////////////////////////////
void LooperBaselinesNoise::get_summaries()
{
    for(auto board : loaded_boards) {
        for(auto chip : loaded_chips) {
            get_summary(board, chip);
            get_noise_summary(board, chip);
        }
        get_noise_summary_board(board);
    }
}

/////////////////////////////////////////////////////////////////////////////
void LooperBaselinesNoise::get_summary(int board, int chip)
{
    gStyle->SetOptStat(0);

    vector<HolderBaselinesNoise> bc_holder;
    for(auto b : loaded_boards) {
        if(b!=board) continue;
        for(auto c : loaded_chips) {
            if(c!=chip) continue;
            for(auto ch : loaded_channels) {
                BoardChipChannelTuple tuple(b, c, ch);
                bc_holder.push_back(holder[tuple]);
            } // ch
        } // c
    } // b

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip;

    stringstream n;
    n << "c_baselines_summary" << coordstr.str();

    TCanvas* c0 = new TCanvas(n.str().c_str(), "", 1200, 800);
    c0->SetGrid(1,1);
    c0->cd();

    n.str("");
    n << "h_baselines_summary" << coordstr.str();

    int n_bins = loaded_channels.size();
    int bin_low = loaded_channels.at(0);
    int bin_high = loaded_channels.at(loaded_channels.size()-1);

    stringstream title;
    title << "Baselines Summary for [board,chip]=["<<board<<","<<chip<<"]";
    TH1F* h0 = new TH1F(n.str().c_str(), title.str().c_str(), n_bins, bin_low, bin_high);

    float miny = 5000;
    double mean = 0.0;
    int n_count = 0;

    for(auto data : bc_holder) {
        int channel = data.channel;
        float value = data.baseline;
        value = x_to_mv((float)value);
        h0->SetBinContent(channel+1, value);
        float value_error = data.baseline_error;
        value_error = x_to_mv(value_error);
        h0->SetBinError(channel+1, value_error);

        if( (value - value_error) < miny) miny = ( value - value_error);
        mean += value;
        n_count++;
    } // data

    // find high leverage points
    mean = (mean / n_count);
    vector<int> bad_channels;
    for(auto data : bc_holder) {
        float value = data.baseline;
        value = x_to_mv(value);

        float num = (value - mean);
        num = (num * num);

        float den = 0.0;
        for(auto data2 : bc_holder) {
            float value2 = data2.baseline;
            value2 = x_to_mv(value2);
            float d = (value2 - mean);
            d = (d*d);
            den += d;
        } // data2

        float lev = (1.0 / n_count) + (num / den);
        if(lev >= 0.2) {
            if(dbg()) {
                cout << "vmmcalib    WARNING Detecting bad (high leverage) point"
                    << " at [board,chip,channel]=["<<data.board<<","<<data.chip<<","<<data.channel<<"]" << endl;
            }
            bad_channels.push_back(data.channel);
        }
    } // data

    BoardChipPair pair(board, chip);
    bad_channel_holder[pair] = bad_channels;

    vector<float> values_for_spread;

    title.str("");
    title << "Baselines Summary for [board,chip]=["<<board<<","<<chip<<"]";
    n << "_bad";
    TH1F* hbad = new TH1F(n.str().c_str(), title.str().c_str(), n_bins, bin_low, bin_high);
    //if(bad_channels.size() > 0) {
        for(auto data : bc_holder) {
            int channel = data.channel;
            bool is_bad = false;
            if(bad_channels.size()>0) {
                is_bad = (std::find(bad_channels.begin(), bad_channels.end(), channel) != bad_channels.end());
            }
            double value = data.baseline;
            value = x_to_mv((float)value);
            if(!is_bad) {
                values_for_spread.push_back(value);
                value = -10;
            }
            double value_error = data.baseline_error;
            value_error = x_to_mv((float)value_error); 
            hbad->SetBinContent(channel+1, value);
            hbad->SetBinError(channel+1, value_error);
        } // data
    //}

    n.str("");
    n << "spread" << coordstr.str();

    TH1F* hspread = new TH1F(n.str().c_str(), "", 5000, 0, 5000);
    for(auto x : values_for_spread) hspread->Fill(x);
    double summary_mean = hspread->GetMean();
    double summary_spread = hspread->GetRMS();

    BoardChipPair bcpair(board, chip);
    std::pair<float, float> summary_pair = std::make_pair(summary_mean, summary_spread);
    summary_holder[bcpair] = summary_pair;

    // draw
    h0->SetMarkerStyle(20);
    h0->SetMarkerSize(1.5 * h0->GetMarkerSize());
    h0->GetXaxis()->SetTitle("VMM Channel");
    h0->GetXaxis()->SetTitleFont(42);
    h0->GetXaxis()->SetLabelFont(42);
    title.str("");
    h0->GetYaxis()->SetTitle("Baseline [mV]");
    h0->GetYaxis()->SetTitleFont(42);
    h0->GetYaxis()->SetLabelFont(42);

    hbad->SetMarkerStyle(20);
    hbad->SetMarkerSize(1.5 * hbad->GetMarkerSize());
    hbad->SetMarkerColor(kRed);
    hbad->GetXaxis()->SetTitle("VMM Channel");
    hbad->GetXaxis()->SetTitleFont(42);
    hbad->GetXaxis()->SetLabelFont(42);
    hbad->GetYaxis()->SetTitle("Baseline [mV]");
    hbad->GetYaxis()->SetTitleFont(42);
    hbad->GetYaxis()->SetLabelFont(42);

    double maxy = h0->GetMaximum();
    maxy = 1.15 * maxy;
    miny = 0.95 * miny;

    h0->SetMaximum(maxy);
    h0->SetMinimum(miny);
    hbad->SetMaximum(maxy);
    hbad->SetMinimum(miny);

    TLine* mean_line = new TLine(bin_low, summary_mean, bin_high, summary_mean);
    mean_line->SetLineColor(kBlue);
    mean_line->SetLineWidth(1);

    TLine* line_up = new TLine(bin_low, summary_mean + summary_spread, bin_high, summary_mean + summary_spread);
    line_up->SetLineColor(kRed);
    line_up->SetLineStyle(2);
    line_up->SetLineWidth(1);

    TLine* line_dn = new TLine(bin_low, summary_mean - summary_spread, bin_high, summary_mean - summary_spread);
    line_dn->SetLineColor(kRed);
    line_dn->SetLineStyle(2);
    line_dn->SetLineWidth(1);

    c0->cd();
    h0->Draw("x0 p e");
    c0->Update();
    hbad->Draw("same x0 p e");
    c0->Update();
    mean_line->Draw();
    line_up->Draw();
    line_dn->Draw();
    c0->Update();

    TLatex text;
    double x_pos = (bin_low + 1);
    double y_pos = 0.95 * maxy;
    text.SetTextFont(42);
    n.str("");
    n << "Mean: " << std::setprecision(4) << summary_mean << " #pm " << summary_spread << " mV";
    text.DrawLatex(x_pos, y_pos, n.str().c_str());
    c0->Update();

    // save
    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "baselines_summary" << coordstr.str();
    if(name() != "") n << "_" << name();
    n << ".pdf";
    c0->SaveAs(n.str().c_str());
    
    
    
    

}

/////////////////////////////////////////////////////////////////////////////
void LooperBaselinesNoise::get_noise_summary(int board, int chip)
{
    vector<HolderBaselinesNoise> bc_holder;
    for(auto b : loaded_boards) {
        if(b!=board) continue;
        for(auto c : loaded_chips) {
            if(c!= chip) continue;
            for(auto ch : loaded_channels) {
                BoardChipChannelTuple tuple(b, c, ch);
                bc_holder.push_back(holder[tuple]);
            } // ch 
        } // c
    } // b

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip;

    stringstream n;
    n << "c_noise_summary" << coordstr.str();

    TCanvas* c0 = new TCanvas(n.str().c_str(), "", 1200, 800);
    c0->SetGrid(1,1);
    c0->cd();

    n.str("");
    n << "h_noise_summary" << coordstr.str();

    int n_bins = loaded_channels.size();
    int bin_low = loaded_channels.at(0);
    int bin_high = loaded_channels.at(loaded_channels.size()-1);

    stringstream title;
    title << "Channel Noise Summary for [board,chip]=["<<board<<","<<chip<<"]";
    TH1F* h0 = new TH1F(n.str().c_str(), title.str().c_str(), n_bins, bin_low, bin_high);

    float miny = 5000;
    double mean = 0.0;
    int n_count = 0;

    vector<float> values_for_spread;
    for(auto data : bc_holder) {
        int channel = data.channel;
        float value = data.baseline_error;
        value = x_to_mv(value);
        values_for_spread.push_back(value);

        h0->SetBinContent(channel+1, value);
    }

    TH1F* hspread = (TH1F*)h0->Clone("spread");
    for(auto x : values_for_spread) hspread->Fill(x);

    float avg = hspread->GetMean();
    float spread = hspread->GetRMS();

    // draw
    h0->SetMarkerStyle(20);
    h0->SetMarkerSize(1.5 * h0->GetMarkerSize());
    h0->GetXaxis()->SetTitle("VMM Channel");
    h0->GetXaxis()->SetTitleFont(42);
    h0->GetXaxis()->SetLabelFont(42);
    h0->GetYaxis()->SetTitle("Channel Noise [mV]");
    h0->GetYaxis()->SetTitleFont(42);
    h0->GetYaxis()->SetLabelFont(42);

    float maxy = h0->GetMaximum();
    maxy = 1.1 * maxy;
    miny = 0.95 * h0->GetMinimum();
    h0->SetMaximum(maxy);
    h0->SetMinimum(miny);

    TLine* mean_line = new TLine(bin_low, avg, bin_high, avg);
    mean_line->SetLineColor(kBlue);
    mean_line->SetLineWidth(1);

    TLine* line_up = new TLine(bin_low, avg + spread, bin_high, avg + spread);
    line_up->SetLineColor(kRed);
    line_up->SetLineStyle(2);
    line_up->SetLineWidth(1);

    TLine* line_dn = new TLine(bin_low, avg - spread, bin_high, avg - spread);
    line_dn->SetLineColor(kRed);
    line_dn->SetLineStyle(2);
    line_dn->SetLineWidth(1);

    c0->cd();
    h0->Draw("x0 p");
    c0->Update();
    mean_line->Draw();
    line_up->Draw();
    line_dn->Draw();
    c0->Update();

    TLatex text;
    double x_pos = (bin_low + 1);
    double y_pos = 0.95 * maxy;
    text.SetTextFont(42);
    n.str("");

    n << "Mean: " << std::setprecision(4) << avg << " #pm " << spread << " mV";
    text.DrawLatex(x_pos, y_pos, n.str().c_str());
    c0->Update();

    // save
    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "noise_summary" << coordstr.str();
    if(name() != "") n << "_" << name();
    n << ".pdf";
    c0->SaveAs(n.str().c_str());

}
/////////////////////////////////////////////////////////////////////////////
void LooperBaselinesNoise::get_noise_summary_board(int board)
{
    vector<HolderBaselinesNoise> bc_holder;
    for(auto b : loaded_boards) {
        if(b!=board) continue;
        for(auto c : loaded_chips) {
            for(auto ch : loaded_channels) {
                BoardChipChannelTuple tuple(b, c, ch);
                bc_holder.push_back(holder[tuple]);
            } //ch
        } // c
    } // b

    stringstream coordstr;
    coordstr << "_b" << board;

    stringstream n;
    n << "c_noise_summary_board" << coordstr.str();

    TCanvas* c0 = new TCanvas(n.str().c_str(), "", 1200, 800);
    c0->SetGrid(1,1);
    c0->cd();

    gPad->SetRightMargin(1.40*gPad->GetRightMargin());

    n.str("");
    n << "h_board_noise_summary" << coordstr.str();

    int nbins_x = loaded_channels.size();
    int bin_low_x = loaded_channels.at(0);
    int bin_high_x = loaded_channels.at(loaded_channels.size() - 1);

    int nbins_y = loaded_chips.size();
    int bin_low_y = loaded_chips.at(0);
    int bin_high_y = loaded_chips.size();

    gStyle->SetNumberContours(700);

    stringstream title;
    title << "Channel Noise Summary for Board = " << board;
    TH2F* h0 = new TH2F(n.str().c_str(), title.str().c_str(), nbins_x, bin_low_x, bin_high_x, nbins_y, bin_low_y, bin_high_y);

    for(int ic = 0; ic < (int)loaded_chips.size(); ic++) {
        n.str("");
        n << ic;
        h0->GetYaxis()->SetBinLabel(ic+1, n.str().c_str());
    } // ic
    h0->GetYaxis()->SetTitleOffset(1.06*h0->GetYaxis()->GetTitleOffset());
    n.str("");

    float maxz = -1;
    float minz = 10000;
    for(auto data : bc_holder) {
        int x = data.channel;
        int y = data.chip;
        float z = data.baseline_error;
        z = x_to_mv(z);

        if(z > maxz) maxz = z;
        if(z < minz) minz = z; 
        h0->SetBinContent(x+1,y+1, z);
    }

    // draw
    h0->GetXaxis()->SetTitle("VMM Channel");
    h0->GetXaxis()->SetTitleFont(42);
    h0->GetXaxis()->SetLabelFont(42);
    h0->GetYaxis()->SetTitle("VMM #");
    h0->GetYaxis()->SetTitleFont(42);
    h0->GetYaxis()->SetLabelFont(42);

    h0->GetZaxis()->SetLabelSize(0.85 * h0->GetZaxis()->GetLabelSize());
    h0->SetMaximum(1.15*maxz);
    h0->SetMinimum(0.5*minz);

    c0->cd();
    h0->Draw("colz");
    c0->Update();

    TLatex text;
    double x_pos = 0.97;
    double y_pos = 0.52;
    text.SetTextFont(42);
    text.SetTextAngle(90);
    text.SetTextSize(0.85 * text.GetTextSize());
    n.str("");
    n << "Channel Noise [mV]";
    text.DrawLatexNDC(x_pos, y_pos, n.str().c_str());
    n.str("");
    c0->Update();

    // save
    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "noise_summary_board" << coordstr.str();
    if(name()!="") n << "_" << name();
    n << ".pdf";
    c0->SaveAs(n.str().c_str());
}
/////////////////////////////////////////////////////////////////////////////
void LooperBaselinesNoise::write_txt_file()
{
    stringstream n;
    n << data_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "baselines_noise_data";
    if(name()!="") n << "_" << name();
    n << ".txt";

    std::ofstream ofs;
    ofs.open(n.str(), std::ofstream::out);

    //HEADER
    std::string dsplit = "\t\t\t";
    ofs << "#BOARD" << dsplit << "CHIP" << dsplit << "CHANNEL"
            << dsplit << "BASELINE[MV]" << dsplit << "NOISE[MV]" << dsplit
            << "BASELINE[CTS]" << dsplit << "NOISE[CTS]" << "BAD\n";

    for(int iboard = 0; iboard < (int)loaded_boards.size(); iboard++) {
        int board = loaded_boards.at(iboard);
    for(int ichip = 0; ichip < (int)loaded_chips.size(); ichip++) {
        int chip = loaded_chips.at(ichip);
        BoardChipPair bcpair(board, chip);

        // summary info line
      //  ofs << board << dsplit << chip << dsplit << "X" << dsplit << std::setprecision(4)
      //          << std::get<0>(summary_holder[bcpair]) << dsplit << std::get<1>(summary_holder[bcpair])
      //          << dsplit << "X\n";
    for(int ichan = 0; ichan < (int)loaded_channels.size(); ichan++) {
        int chan = loaded_channels.at(ichan);
        BoardChipChannelTuple tuple(board, chip, chan);
        float baseline_cts = holder[tuple].baseline;
        float baseline = x_to_mv(baseline_cts);
        float baseline_error_cts = holder[tuple].baseline_error;
        float baseline_error = x_to_mv(baseline_error_cts);
        ofs << board << dsplit << chip << dsplit << chan << dsplit
            << std::setprecision(4)
            << baseline << dsplit << baseline_error
            << dsplit << baseline_cts << dsplit << baseline_error_cts;

        bool is_bad = false;
        for(auto bad_channels : bad_channel_holder[bcpair]) {
            if(bad_channels == chan) is_bad = true;
        }

        ofs << dsplit << is_bad << "\n";
    } // ichan 
    } // ichip
    } // iboard

    ofs.close();

    cout << "vmmcalib    INFO Channel baseline and noise data stored in file: " << n.str() << endl;

}
