#include "calib_base.h"
using namespace vmmcalib;

//std/stl
#include <fstream>
#include <iostream>
#include <sys/stat.h>
using namespace std;

//ROOT
#include "TChain.h"
#include "TFile.h"
#include "TBranch.h"

//////////////////////////////////////////////////////////////////////////////
CalibBase::CalibBase() :
    m_dbg(0),
    m_type(CalibrationType::Invalid),
    m_events_gathered(0.0),
    m_subtract_baselines(false),
    m_select_board(-1),
    m_select_chip(-1),
    m_select_channel(-1),
    m_n_chan_override(-1),
    m_select_gain(-1.0),
    m_select_peak_time(-1),
    m_select_pulser(-1),
    m_select_threshold(-1),
    m_select_tac(-1),
    m_tfile(nullptr),
    m_chain(nullptr),
    m_name(""),
    m_output_dir("./"),
    m_plot_dir("./"),
    m_data_dir("./"),
    m_do_all_plots(false)
{
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::clear()
{
    events.clear();
    loaded_boards.clear();
    loaded_chips.clear();
    loaded_channels.clear();
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::set_output_dir(string dirname)
{
    int status = mkdir(dirname.c_str(), 0755);
    bool dir_ok = true;
    if( status<0 && errno == EEXIST ) {
        cout << "vmmcalib    INFO Using pre-existing output directory (" << dirname << "), files"
            << " may be over-written" << endl;
        m_output_dir = dirname;
    }
    else if(status!=0) {
        dir_ok = false;
        cout << "vmmcalib    ERROR Requested output directory (" << dirname << ") bad, setting to default" << endl;
        m_output_dir = "./";
    }
    if(!dir_ok) {
        m_output_dir = "./";
        m_plot_dir = "./";
        m_data_dir = "./";
        return;
    }

    m_output_dir = dirname;
    char last = m_output_dir.back();
    if(last != '/') {
        m_output_dir = m_output_dir + "/";
    }

    bool sub_ok = true;
    m_plot_dir = m_output_dir + "plots"; 
    if(name()!="") m_plot_dir = m_plot_dir + "_" + name() + "/";
    status = mkdir(m_plot_dir.c_str(), 0755);
    if( status < 0 && errno == EEXIST ) {
        cout << "vmmcalib    INFO Using pre-existing plot directory (" << m_plot_dir << ")" << endl;
    }
    else if(status != 0) {
        sub_ok = false;
    }

    m_data_dir = m_output_dir + "data";
    if(name()!="") m_data_dir = m_data_dir + "_" + name() + "/";
    status = mkdir(m_data_dir.c_str(), 0755);
    if( status < 0 && errno == EEXIST ) {
        cout << "vmmcalib    INFO Using pre-existing data directory (" << m_data_dir << ")" << endl;
    }
    else if(status != 0) {
        sub_ok = false;
    }
    if(!sub_ok) {
        cout << "vmmcalib    WARNING Sub-dirs for plots and data (" << m_plot_dir << " or " << m_data_dir << ") are bad, putting output in " << m_output_dir << endl;
        m_plot_dir = m_output_dir;
        m_data_dir = m_output_dir;
    }
}
//////////////////////////////////////////////////////////////////////////////
bool CalibBase::connect_tree()
{
    if(vmmcalib::is_xadc(m_type)) {
        m_chain->SetBranchAddress("boardId", &m_board, &br_board);
        m_chain->SetBranchAddress("chip", &m_chip, &br_chip);
        m_chain->SetBranchAddress("channel", &m_channel, &br_channel);
        m_chain->SetBranchAddress("samples", &m_sample, &br_samples);
        m_chain->SetBranchAddress("channelTrim", &m_channel_trim, &br_channel_trim);

        // run properties
        m_chain->SetBranchAddress("pulserCounts", &m_pulser_dac, &br_pulser_dac);
        m_chain->SetBranchAddress("dacCounts", &m_threshold_dac, &br_threshold_dac);
        m_chain->SetBranchAddress("gain", &m_gain, &br_gain);
        m_chain->SetBranchAddress("peakTime", &m_peak_time, &br_peak_time);
        m_chain->SetBranchAddress("tacSlope", &m_tac_slope, &br_tac_slope);
    }
    else {
        return connect_pulser_tree();
    }


    return true;
}
//////////////////////////////////////////////////////////////////////////////
bool CalibBase::connect_pulser_tree()
{
    m_pulser_boards = nullptr;
    m_pulser_channels = nullptr;
    m_pulser_pdo = nullptr;
    m_pulser_tdo = nullptr;
    m_pulser_is_neighbor = nullptr;
    m_pulser_pass_threshold = nullptr;

    m_chain->SetBranchAddress("chip", &m_pulser_chips, &br_pulser_chips);
    m_chain->SetBranchAddress("boardId", &m_pulser_boards, &br_pulser_boards);
    m_chain->SetBranchAddress("channel", &m_pulser_channels, &br_pulser_channels); 
    m_chain->SetBranchAddress("tacSlope", &m_pulser_tac_slope, &br_pulser_tac_slope);
    m_chain->SetBranchAddress("peakTime", &m_pulser_peak_time, &br_pulser_peak_time);
    m_chain->SetBranchAddress("pulserCounts", &m_pulser_pulser_dac, &br_pulser_pulser_dac);
    m_chain->SetBranchAddress("dacCounts", &m_pulser_threshold_dac, &br_pulser_threshold_dac);
    m_chain->SetBranchAddress("tpSkew", &m_pulser_tp_skew, &br_pulser_tp_skew);
    m_chain->SetBranchAddress("pdo", &m_pulser_pdo, &br_pulser_pdo);
    m_chain->SetBranchAddress("tdo", &m_pulser_tdo, &br_pulser_tdo);
    m_chain->SetBranchAddress("isNeighbor", &m_pulser_is_neighbor, &br_pulser_is_neighbor);
    m_chain->SetBranchAddress("threshold", &m_pulser_pass_threshold, &br_pulser_pass_threshold);

    if(type()==vmmcalib::CalibrationType::TimingCKBCDelay) {
        m_chain->SetBranchAddress("bcLatency", &m_pulser_bc_latency, &br_pulser_bc_latency);
    }

    return true;

}
//////////////////////////////////////////////////////////////////////////////
bool CalibBase::load_file(std::string filename)
{
    // we assume here that the file has already been found to exist

    TFile* tmp = TFile::Open(filename.c_str());
    if(tmp) {
        m_tfile = tmp;
        if(dbg()) m_tfile->ls();

        TTree* chain = (TTree*)m_tfile->Get("calib");
        if(chain) {
            m_chain = static_cast<TChain*>(chain);
            bool branches_ok = connect_tree();
            if(!branches_ok) { return false; }
            if(dbg()) { cout << "vmmcalib    calib ntuple with " << m_chain->GetEntriesFast() << " entries" << endl; }
        } // chain
        else {
            delete chain;
            cout << "vmmcalib    ERROR Unable to obtain calib tree from file " << filename << endl;
            return false;
        }
    } // tmp
    else {
        delete tmp;
        cout << "vmmcalib    ERROR Unable to open TFile from file " << filename << endl;
        return false;
    }
    return true;
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::run_calibration()
{
    clear();
    

}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::load_board(int board)
{
    if(!board_loaded(board)) loaded_boards.push_back(board);
    std::sort(loaded_boards.begin(), loaded_boards.end());
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::load_chip(int chip)
{
    if(!chip_loaded(chip)) loaded_chips.push_back(chip);
    std::sort(loaded_chips.begin(), loaded_chips.end());
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::load_channel(int channel)
{
    if(!channel_loaded(channel)) loaded_channels.push_back(channel);
    std::sort(loaded_channels.begin(), loaded_channels.end());
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::load_trim(int trim)
{
    if(!trim_loaded(trim)) loaded_trims.push_back(trim);
    std::sort(loaded_trims.begin(), loaded_trims.end());
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::load_dac(int dac)
{
    if(!dac_loaded(dac)) loaded_dacs.push_back(dac);
    std::sort(loaded_dacs.begin(), loaded_dacs.end());
}
//////////////////////////////////////////////////////////////////////////////
bool CalibBase::board_loaded(int board)
{
    bool is_present = (std::find(loaded_boards.begin(), loaded_boards.end(), board) != loaded_boards.end());
    return is_present;
}
//////////////////////////////////////////////////////////////////////////////
bool CalibBase::chip_loaded(int chip)
{
    bool is_present = (std::find(loaded_chips.begin(), loaded_chips.end(), chip) != loaded_chips.end());
    return is_present;
}
//////////////////////////////////////////////////////////////////////////////
bool CalibBase::channel_loaded(int channel)
{
    bool is_present = (std::find(loaded_channels.begin(), loaded_channels.end(), channel) != loaded_channels.end());
    return is_present;
}
//////////////////////////////////////////////////////////////////////////////
bool CalibBase::trim_loaded(int trim)
{
    bool is_present = (std::find(loaded_trims.begin(), loaded_trims.end(), trim) != loaded_trims.end());
    return is_present;
}
//////////////////////////////////////////////////////////////////////////////
bool CalibBase::dac_loaded(int dac)
{
    bool is_present = (std::find(loaded_dacs.begin(), loaded_dacs.end(), dac) != loaded_dacs.end());
    return is_present;
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::get_events()
{

    bool is_xadc = vmmcalib::is_xadc(m_type);

    if(!is_xadc) { get_pulser_events(); return; }

    m_events_gathered = 0.0;
    Long64_t nentries = chain()->GetEntries();
    Long64_t n_bytes = 0;
    Long64_t nb = 0;
    Long64_t current_entry = 0;
    int update_level = nentries / 20;
    Long64_t for_update = 0;
    int n_checks = 0;

    float previous_gain = 0;
    float previous_pulser = 0;
    float previous_threshold = 0;
    float previous_tac = 0;
    float previous_peak_time = 0;
    bool is_first = true;

    CalibEvent event;

    for(Long64_t jentry = 0; jentry < nentries; jentry++) {

        event.clear();

        nb = chain()->GetEntry(jentry); n_bytes += nb;
        if(jentry % update_level == 0 || jentry == (nentries - 1)) {
            for_update = (jentry + 1);
            cout << "vmmcalib    Gathering events "
                 << std::setw(14) << for_update << " / " << nentries << " ["
                 << std::setw(3) << n_checks*5 << "\%]" << endl;
            n_checks++;
        } // for_update

        current_entry = jentry + 1;

        if( (m_select_board >= 0) && (m_board != m_select_board) ) continue;
        if( (m_select_chip >= 0) && (m_chip != m_select_chip) ) continue;
        if( (m_select_channel >= 0) && (m_channel != m_select_channel) ) continue;


        //if(is_xadc) {
            if( (m_select_gain >= 0) && (m_gain != m_select_gain) ) continue;
            if( (m_select_peak_time >= 0) && (m_peak_time != m_select_peak_time) ) continue;
            if( (m_select_pulser >= 0) && (m_pulser_dac != m_select_pulser) ) continue;
            if( (m_select_threshold >= 0) && (m_threshold_dac != m_select_threshold) ) continue;
            if( (m_select_tac >= 0) && (m_tac_slope != m_select_tac) ) continue;

        //}

        if(is_first) {
            is_first = false;
            previous_gain = m_gain;
            previous_pulser = m_pulser_dac;
            previous_threshold = m_threshold_dac;
            previous_tac = m_tac_slope;
            previous_peak_time = m_peak_time;
        }
        else {
            if(m_gain != previous_gain) {
                cout << "vmmcalib    ERROR Multiple gain values encountered, only one is expected. Exiting." << endl;
                exit(1);
            }
            if(m_peak_time != previous_peak_time) {
                cout << "vmmcalib    ERROR Multiple peak time values encountered, only one is expected. Exiting." << endl;
                exit(1);
            }
            if(m_pulser_dac != previous_pulser) {
                cout << "vmmcalib    ERROR Multiple pulser DAC values encountered, only one is expected. Exiting." << endl;
                exit(1);
            }
            if(m_threshold_dac != previous_threshold) {
                cout << "vmmcalib    ERROR Multiple threshold DAC values encountered, only one is expected. Exiting." << endl;
                exit(1);
            }
            if(m_tac_slope != previous_tac) {
                cout << "vmmcalib    ERROR Multiple TAC slope values encountered, only one is expected. Exiting." << endl;
                exit(1);
            }

            previous_gain = m_gain;
            previous_peak_time = m_peak_time;
            previous_pulser = m_pulser_dac;
            previous_threshold = m_threshold_dac;
            previous_tac = m_tac_slope;
        }

        // Get the data
        event.board = m_board;
        event.chip = m_chip;
        event.channel = m_channel;

        //if(m_channel > 1) continue;

        load_board(event.board);
        load_chip(event.chip);
        load_channel(event.channel);

        event.threshold_dac = m_threshold_dac;
        event.pulser_dac = m_pulser_dac;
        event.gain = m_gain;
        event.peak_time = m_peak_time;
        event.tac_slope = m_tac_slope;

        if(is_xadc) {
            event.sample = m_sample;
            event.channel_trim = m_channel_trim;
            load_trim(event.channel_trim);
            if(type()==vmmcalib::CalibrationType::PulserDACXADC) load_dac(event.pulser_dac); 
            else if(type()==vmmcalib::CalibrationType::ThresholdDACXADC) load_dac(event.threshold_dac);
        }
        else {
            cout << "vmmcalib    WARNING Attempting to gather non-xadc event data, this is not ready!" << endl;
        }


        BoardChipChannelTuple tuple(event.board, event.chip, event.channel);
        channel_counts[tuple][event.channel_trim]++;

        if(type()==vmmcalib::CalibrationType::ChannelTrimsXADC && (n_chan_override() >= 0) && !(channel_counts[tuple][event.channel_trim] > n_chan_override())) {
            events[tuple].push_back(event);
            m_events_gathered++;
        }
        else if(n_chan_override()<0) {
            events[tuple].push_back(event);
            m_events_gathered++;
        }

    } // jentry

    cout << "-----------------------------------------------------------------" << endl;

    if(m_events_gathered == 0.0) {
        cout << " vmmcalib    NO events gathered!" << endl;
        exit(0);
    }


    cout << " vmmcalib    Events gathered" << endl;
    cout << " > loaded boards:    [";
    for(auto board : loaded_boards) cout << " " << board;
    cout << " ]" << endl;
    cout << " > loaded chips:     [";
    for(auto chip : loaded_chips)  cout << " " << chip;
    cout << " ]" << endl;
    cout << " > loaded channels:  [";
    for(auto channel : loaded_channels) cout << " " << channel;
    cout << " ]" << endl;
    if(is_xadc && m_type == vmmcalib::CalibrationType::ChannelTrimsXADC) {
    cout << " > loaded trims:     [";
    for(auto trim : loaded_trims) cout << " " << trim;
    cout << " ]" << endl;
    }
    cout << "-----------------------------------------------------------------" << endl;



    return;
}
//////////////////////////////////////////////////////////////////////////////
void CalibBase::get_pulser_events()
{
    cout << "get_pulser_events" << endl;

    m_events_gathered = 0.0;
    Long64_t current_entry = 0;
    Long64_t nb = 0;
    Long64_t nentries = chain()->GetEntriesFast();
    int update_level = nentries / 20;
    Long64_t for_update = 0;
    int n_checks = 0;

    for(Long64_t jentry = 0; jentry < nentries; jentry++) {
        nb = chain()->GetEntry(jentry);
        if(jentry % update_level == 0 || jentry==(nentries - 1) ) {
            for_update = (jentry + 1);
            cout << "vmmcalib    Gathering events "
                << std::setw(14) << for_update << " / " << nentries << " ["
                << std::setw(3) << n_checks*5 << "\%]" << endl;
            n_checks++;
        }

        // build up the hits for this event
        current_entry = jentry + 1;
        size_t number_of_chips = m_pulser_tdo->size();
        cout << "# of chips = " << number_of_chips << endl;
        


    } // jentry


}
