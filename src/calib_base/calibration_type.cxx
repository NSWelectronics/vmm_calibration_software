#include "calibration_type.h"
using namespace vmmcalib;

#include <sstream>

std::string vmmcalib::CalibrationType2Str(const CalibrationType& c)
{
    std::string s = "Invalid";
    switch(c) {
        case CalibrationType::BaselinesXADC         : s = "BaselinesXADC"; break;
        case CalibrationType::ChannelTrimsXADC      : s = "ChannelTrimsXADC"; break;
        case CalibrationType::PulserDACXADC         : s = "PulserDACXADC"; break;
        case CalibrationType::ThresholdDACXADC      : s = "ThresholdDACXADC"; break;
        case CalibrationType::GainsPedestal         : s = "GainsPedestal"; break;
        case CalibrationType::TimingCKBCDelay       : s = "TimingCKBCDelay";  break;
        case CalibrationType::Invalid               : s = "Invalid"; break;
    }
    return s;
}

CalibrationType vmmcalib::CalibrationTypeFromStr(const std::string& c)
{
    CalibrationType out = CalibrationType::Invalid;
    if(c=="BaselinesXADC") out = CalibrationType::BaselinesXADC;
    else if(c=="ChannelTrimsXADC") out = CalibrationType::ChannelTrimsXADC;
    else if(c=="PulserDACXADC") out = CalibrationType::PulserDACXADC;
    else if(c=="ThresholdDACXADC") out = CalibrationType::ThresholdDACXADC;
    else if(c=="GainsPedestal") out = CalibrationType::GainsPedestal;
    else if(c=="TimingCKBCDelay") out = CalibrationType::TimingCKBCDelay;
    return out;
}

bool vmmcalib::is_valid_type(std::string name)
{
    if(vmmcalib::CalibrationTypeFromStr(name) == CalibrationType::Invalid) return false;
    return true;
}

std::string vmmcalib::valid_types()
{
    std::stringstream out;
    out << "[1] BaselinesXADC";
    out << ", [2] ChannelTrimsXADC";
    out << ", [3] PulserDACXADC";
    out << ", [4] ThresholdDACXADC";
    out << ", [5] GainsPedestal";
    out << ", [6] TimingCKBCDelay";
    return out.str();
}

bool vmmcalib::is_xadc(const CalibrationType& c)
{
    if(c == vmmcalib::CalibrationType::BaselinesXADC ||
       c == vmmcalib::CalibrationType::ChannelTrimsXADC ||
       c == vmmcalib::CalibrationType::PulserDACXADC ||
       c == vmmcalib::CalibrationType::ThresholdDACXADC) {
        return true;
    }
    return false;
}
