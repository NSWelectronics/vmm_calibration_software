#include "looper_channel_trims.h"
using namespace vmmcalib;

//std/stl
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
using namespace std;

//ROOT
#include "TStyle.h"
#include "TLegend.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TLatex.h"
#include "TLine.h"
#include "TGaxis.h"
#include "TPad.h"

//boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

/////////////////////////////////////////////////////////////////////////////
LooperChannelTrims::LooperChannelTrims()
{
    cout << " >>> LooperChannelTrims" << endl;
}
/////////////////////////////////////////////////////////////////////////////
bool LooperChannelTrims::load_baselines(string filename)
{
    std::ifstream ifs(filename.c_str());
    int board = 0;
    int chip = 0;
    int channel = 0;
    float baseline = 0.0;
    float baseline_error = 0.0;
    float baseline_cts = 0.0;
    float baseline_error_cts = 0.0;
    int is_bad = 0;

    float baseline_avg = 0.0;
    float baseline_error_avg = 0.0;
    float baseline_cts_avg = 0.0;
    float baseline_error_cts_avg = 0.0;
    int n_read_in = 0;
    map<BoardChipChannelTuple, int> bad_baselines;

    try {
    // first line is header, get it out of the way
    string line;
    getline(ifs, line);

    // now read the rest
    while(ifs >> board >> chip >> channel >> baseline >> baseline_error >> baseline_cts >> baseline_error_cts >> is_bad) {
        //cout << board << " " << chip << " " << channel << " " << baseline
        //    << " " << baseline_error << " " << baseline_cts << " " << baseline_error_cts << is_bad << endl;
        BoardChipChannelTuple tuple(board, chip, channel);
        baselines_holder[tuple] = baseline_cts;
        if(is_bad==1) bad_baselines[tuple] = 1;
        else { 
            bad_baselines[tuple] = 0;
            baseline_avg += baseline;
            baseline_error_avg += baseline_error;
            baseline_cts_avg += baseline_cts;
            baseline_error_cts_avg += baseline_error_cts;
            n_read_in++;
        }
    }
    }
    catch(std::exception e) {
        cout << "vmmcalib    ERROR " << e.what() << endl;
        return false;
    }

    // calc average
    baseline_avg = (baseline_avg / n_read_in);
    baseline_error_avg = (baseline_error_avg / n_read_in);
    baseline_cts_avg = (baseline_cts_avg / n_read_in);
    baseline_error_cts_avg = (baseline_error_cts_avg / n_read_in);

    // for the "bad" baselines, set the value that will be subtracted to the global average
    for(auto & x : bad_baselines) {
        baselines_holder[x.first] = baseline_cts_avg;

    }

    cout << "vmmcalib    INFO Baselines loaded" << endl;
    m_subtract_baselines = true;
    return true;

}
/////////////////////////////////////////////////////////////////////////////
bool LooperChannelTrims::load_threshold_calibration(std::string filename)
{
    threshold_calib_holder.clear();

    std::ifstream ifs(filename.c_str());

    int board = 0;
    int chip = 0;

    float slope = 0.0; // mV/cts
    float slope_error = 0.0; // mV/cts

    float constant = 0.0; // mV
    float constant_error = 0.0; // mV

    // first line is hearder, get it out of the way
    try {
    string line;
    getline(ifs, line);

    while(ifs >> board >> chip >> slope >> slope_error >> constant >> constant_error) 
    {
        //cout << board << " " << chip << " " << slope << endl;
        BoardChipPair bcpair(board, chip);
        std::pair<float, float> dac_pair = std::make_pair(slope, constant);
        threshold_calib_holder[bcpair] = dac_pair;
    }

    //for(auto x : threshold_calib_holder) {
    //    BoardChipPair bcpair = x.first;
    //    cout << "b c = " << std::get<0>(bcpair) << " " << std::get<1>(bcpair)
    //        << "  slope = " << std::get<0>(x.second) << "  const = " << std::get<1>(x.second) << endl;
    //}

    }
    catch(std::exception &e) {
        cout << "vmmcalib    ERROR : " << e.what() << endl;
        m_threshold_calib_loaded = false;
        return false;
    }
    m_threshold_calib_loaded = true;

    return true;

}
/////////////////////////////////////////////////////////////////////////////
float LooperChannelTrims::calculate_threshold_dac(int board, int chip)
{

    if(!m_threshold_calib_loaded) return -1.0;

    BoardChipPair bcpair(board, chip);
    std::pair<float, float> dac_pair = threshold_calib_holder[bcpair];

    int dac_set = -1;
    for(auto x : events) {
        dac_set = x.second.at(0).threshold_dac;
        break;
    }
    if(dac_set < 0) return -1.0;

    float slope = std::get<0>(dac_pair);
    float constant = std::get<1>(dac_pair);

    return ( slope * dac_set + constant );

}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::run_calibration()
{
    get_events(); // c.f. vmm_calibration_software/calib_base.h

    get_channel_thresholds_and_ranges();

    // determine the best trimmer settings
    get_best_trims();

    make_summary_plots();

    write_txt_file();

    write_xml_file();

}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::get_channel_thresholds_and_ranges()
{
    float n_total = 0;
    for(auto b : loaded_boards) {
        for(auto chip : loaded_chips) {
            for(auto chan : loaded_channels) {
                n_total++;
            }
        }   
    }

    // collect all of the data
    float n_at = 0;
    for(auto board : loaded_boards) {
        for(auto chip : loaded_chips) {
            for(auto channel : loaded_channels) {
                cout << "vmmcalib    Getting channel thresholds and trimmer ranges [board,chip,channel]=["<<board<<","<<chip<<","<<channel<<"]" 
                << " " << std::setw(6) << std::setprecision(4) << (n_at / n_total) * 100.  << " \% " << endl; //" (at = " << n_at << "  total = " << n_total << ")" << endl;
                get_threshold_per_trim(board, chip, channel);
                get_threshold_range(board, chip, channel);
                n_at++;
            } // channel
        } // chip
    } // board
}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::get_threshold_per_trim(int board, int chip, int channel)
{

    HolderChannelTrims hch; 
    hch.board = board;
    hch.chip = chip;
    hch.channel = channel;

    stringstream bcpstr;
    bcpstr << "[board,chip,channel]=[" << board << "," << chip << "," << channel << "]";

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip << "_ch" << channel;

    stringstream n;
    n << "c_thresholds_per_trim_" << coordstr.str();
    

    TCanvas* c0 = new TCanvas( n.str().c_str(), "", 800, 600);
    c0->SetGrid(1,1);
    c0->cd();

    stringstream title;
    title << "Channel Thresholds per Trim " << bcpstr.str(); 

    vector<TH1F*> histos;
    float max_x = -1;
    float min_x = 5000;
    float max_y = -1;
    vector<float> x_text;
    vector<float> y_text;

    float last_mean = 0.0;
    bool is_first_mean = true;
    vector<int> fills;

    for(auto trim : loaded_trims) {
        n.str("");
        n << "h_trims_" << coordstr.str() << "_trim" << trim;
        TH1F* h = new TH1F(n.str().c_str(), title.str().c_str(), 4096, 0, 4096);
        for(auto hx : events) {
            int b = std::get<0>(hx.first);
            int c = std::get<1>(hx.first);
            int ch = std::get<2>(hx.first);
            if(!(b==board && c==chip && ch==channel)) continue;
            BoardChipChannelTuple tuple(b,c,ch);
            vector<CalibEvent> ev = events[tuple];

            for(auto e : ev) {
                int tr = e.channel_trim;
                if(tr != trim) continue;

                float xadc_sample = e.sample;
                if(m_subtract_baselines) {
                    xadc_sample = (xadc_sample - baselines_holder[tuple]);
                }

                h->Fill(xadc_sample);
                if(xadc_sample > max_x) max_x = xadc_sample;
                if(xadc_sample < min_x) min_x = xadc_sample;
            } // e
            y_text.push_back(h->GetMaximum());
            if(h->GetMaximum() > max_y) max_y = h->GetMaximum();
            histos.push_back(h);

            double mean_threshold = h->GetMean();
            double threshold_err = h->GetRMS();

            if(is_first_mean) {
                last_mean = mean_threshold;
                is_first_mean = false;
                fills.push_back(0);
            }
            else {
                if(mean_threshold > last_mean) { fills.push_back(1); }
                else { fills.push_back(0); }
                last_mean = mean_threshold;
            }

            x_text.push_back(mean_threshold);
            std::pair<float, float> te_pair ( mean_threshold, threshold_err );
            hch.trim_map[trim] = te_pair;

            // load this to the holder
            holder[tuple] = hch;

        } // hx

    } // trim

    ///////////////////////////////////////////////////////////////////
    // drawing
    ///////////////////////////////////////////////////////////////////
    if(all_plots()) {
        gStyle->SetOptStat(0);
        c0->SetFillColor(0);

        max_x = max_x + 10;
        min_x = min_x - 10;
        max_y = 1.23 * max_y;

        for(auto & hist : histos) {
            hist->GetXaxis()->SetRangeUser(min_x, max_x);
            hist->SetMaximum(max_y);
        } // hist

        bool is_first = true;
        int h_idx = 0;
        for(auto & hist : histos) {
            hist->SetLineColor(kBlue);
            if(fills.at(h_idx) == 1) {
                hist->SetFillStyle(1001);
                hist->SetLineColor(kRed);
                hist->SetFillColor(kRed);
            }

            hist->GetXaxis()->SetTitle("Channel Threshold [counts]");
            hist->GetXaxis()->SetTitleFont(42);
            hist->GetXaxis()->SetLabelFont(42);
            hist->GetYaxis()->SetTitle("Entries");
            hist->GetYaxis()->SetTitleFont(42);
            hist->GetYaxis()->SetLabelFont(42);
            h_idx++;

            if(is_first) { hist->Draw("hist"); is_first = false; }
            else hist->Draw("hist same");
        } // hist

        TLatex text;
        text.SetTextFont(42);
        text.SetTextSize(0.3*text.GetTextSize());
        for(int i = 0; i < (int) x_text.size(); i++) {
            n.str("");
            n << i;
            text.DrawLatex(x_text.at(i), 1.02 * y_text.at(i), n.str().c_str());
        } // i

        n.str("");
        n << plot_dir();
        char last = n.str().back();
        if(last != '/') n << "/";
        n << "channel_trims_samples_" << coordstr.str(); 
        if(name() != "") n << "_" << name();
        if(m_subtract_baselines) n << "_BS";
        n << ".pdf";
        c0->SaveAs(n.str().c_str());
        delete c0;

    } // all_plots


}
/////////////////////////////////////////////////////////////////////////////
float LooperChannelTrims::x_to_mv(float counts)
{
    return (( counts / pow(2,12)) * 1000.);
}
float LooperChannelTrims::x_to_mv(int counts)
{
    return (( (1.0 * counts) / pow(2,12)) * 1000.);
}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::get_threshold_range(int board, int chip, int channel)
{
    BoardChipChannelTuple tuple(board, chip, channel);
    HolderChannelTrims hch = holder[tuple];

    vector<int> trims;
    vector<float> thresholds;

    for(auto trim : loaded_trims) { // loaded_trims is ordered
        for(auto tmap : hch.trim_map) {
            if(!(tmap.first == trim)) continue;
            float threshold = std::get<0>(tmap.second);

            // convert to mV
            threshold = x_to_mv(threshold);

            trims.push_back(trim);
            thresholds.push_back(threshold);
        } // tmap
    } // trim

    // get the trim that gives the lowest threshold, this defines the
    // range

    int trim = trims.at(0);
    float value = thresholds.at(0);

    for(int i = 1; i < (int)trims.size(); i++) {
        if(thresholds.at(i) < value) {
            value = thresholds.at(i);
            trim = trims.at(i);
        }
    } // i

    hch.full_trim = (trim==31);
    hch.minimum_trim = trim;

    // threshold range values
    float top = std::get<0>(hch.trim_map[trims.at(0)]);
    float top_error = std::get<1>(hch.trim_map[trims.at(0)]);

    float bottom = std::get<0>(hch.trim_map[trim]);
    float bottom_error = std::get<1>(hch.trim_map[trim]);

    float threshold_range = (top + top_error) - (bottom - bottom_error);
    threshold_range = x_to_mv(threshold_range);

    //cout << "b c p " << board << " " << chip << " " << channel << " : "
    //        << "top + top_error = " << top << " + " << top_error
    //        << "   bottom + bottom_error = " << bottom << " + " << bottom_error
    //        << "  -> range = " << threshold_range << endl;
    hch.threshold_range = threshold_range; 

    // just overwrite with the updated one
    holder[tuple] = hch;
}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::get_best_trims()
{
    cout << "vmmcalib    Determining best channel trimmer settings" << endl;
    for(auto b : loaded_boards) {
        cout << "vmmcalib    > board " << b << endl;
        get_best_trims_per_board(b);
        for(auto c : loaded_chips) {
            cout << "vmmcalib      >> chip " << c << endl;
            get_best_trims_per_chip(b, c);
        } // c
    } // b
    cout << "vmmcalib    > global" << endl;
    get_best_trims_global();

}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::get_best_trims_per_chip(int board, int chip)
{
    vector<HolderChannelTrims> hch;

    // get the holders for this board and chip for all channels, ordered by channel
    for(auto ch : loaded_channels) {
        for(auto hx : holder) {
            int b = std::get<0>(hx.first);
            int c = std::get<1>(hx.first);
            int chan = std::get<2>(hx.first);
            if(!(b==board && c==chip && chan==ch)) continue;
            hch.push_back(hx.second);
        } // hx
    } // ch

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip;

    stringstream n;
    n << "h_best_trims_chip" << coordstr.str();
    TH1F* hproj = new TH1F(n.str().c_str(), "", 1024, 0, 4096);
    int max_x = -1;
    int min_x = 5000;
    for(auto h : hch) {
        for(auto trim : loaded_trims) {
            if(trim > h.minimum_trim) continue; // don't go to divergent points
            float threshold = std::get<0>(h.trim_map[trim]);
            hproj->Fill(threshold);
            if(threshold > max_x) max_x = threshold;
            if(threshold < min_x) min_x = threshold;
        } // trim
    } // h

    // most "hit" threshold value (in counts)
    float x_value_max = hproj->GetBinCenter(hproj->GetMaximumBin());

    // find for each channel the trim value that is closest to x_value_max (in counts)
    vector<int> channels;
    vector<int> trims;
    vector<float> distances;

    BoardChipPair bcpair(board, chip);
    vector< pair<int, int> > best_trims;

    for(auto h : hch) {
        float min_distance = 5000;
        int min_trim = 0;
        for(auto trim : loaded_trims) {
            if(trim > h.minimum_trim) continue; // don't go to divergent points
            float threshold_counts = std::get<0>(h.trim_map[trim]);

            // only use nominal value for computing distance, don't get fancy
            float distance = std::abs(threshold_counts - x_value_max);
            if(distance < min_distance) {
                min_distance = distance;
                min_trim = trim;
            }

            trims.push_back(trim);
            distances.push_back(distance);
            channels.push_back(h.channel);

        } // trim

        int best_trim = min_trim;
        int channel = h.channel;
        pair<int, int> best_pair(channel, best_trim);
        best_trims.push_back(best_pair);
    } // h

    // load these to the holder
    best_trims_chip[bcpair] = best_trims;

}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::get_best_trims_per_board(int b)
{
    vector<HolderChannelTrims> hb;

    // get the holders for this board for all chips and channels
    for(auto c : loaded_chips) {
        for(auto ch : loaded_channels) {
            for(auto hx : holder) {
                int board = std::get<0>(hx.first);
                int chip = std::get<1>(hx.first);
                int chan = std::get<2>(hx.first);
                if(!(board==b && chip==c && chan==ch)) continue;
                hb.push_back(hx.second);
            } // hx
        } // ch
    } // c

    stringstream coordstr;
    coordstr << "_b" << b;

    stringstream n;
    n << "h_best_trims_board" << coordstr.str();
    TH1F* hproj = new TH1F(n.str().c_str(), "", 1024, 0, 4096);
    int max_x = -1;
    int min_x = 5000;
    for(auto h : hb) {
        for(auto trim : loaded_trims) {
            if(trim > h.minimum_trim) continue;
            float threshold_counts = std::get<0>(h.trim_map[trim]);
            hproj->Fill(threshold_counts);
            if(threshold_counts > max_x) max_x = threshold_counts;
            if(threshold_counts < min_x) min_x = threshold_counts;
        } // trim
    } // h

    // most hit hits
    float x_value_max = hproj->GetBinCenter(hproj->GetMaximumBin());

    vector< tuple<int, int, int> > best_trims;
    for(auto h : hb) {
        float min_distance = 5000;
        int min_trim = 0;
        for(auto trim : loaded_trims) {
            if(trim > h.minimum_trim) continue;
            float threshold_counts = std::get<0>(h.trim_map[trim]);
            float distance = std::abs(threshold_counts - x_value_max);
            if(distance < min_distance) {
                min_distance = distance;
                min_trim = trim;
            }
        } // trim

        // values for this guy
        int chip = h.chip;
        int channel = h.channel;
        int best_trim = min_trim;
        tuple<int, int, int> best_tuple(chip, channel, best_trim);
        best_trims.push_back(best_tuple);
    } // h

    // load
    for(auto c : loaded_chips) {
        vector< pair<int, int> > best_pairs;
        for(auto best : best_trims) {
            if(!(std::get<0>(best) == c)) continue;
            pair<int, int> best_pair(std::get<1>(best), std::get<2>(best));
            best_pairs.push_back(best_pair);
        } // best
        BoardChipPair bcpair(b,c);
        best_trims_board[bcpair] = best_pairs;
    } // c
}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::get_best_trims_global()
{
    vector<HolderChannelTrims> hg;
    for(auto x : holder) hg.push_back(x.second);

    stringstream coordstr;
    coordstr << "_global";

    stringstream n;
    n << "h_best_trims_global" << coordstr.str();

    TH1F* hproj = new TH1F(n.str().c_str(), "", 1024, 0, 4096);
    int max_x = -1;
    int min_x = 5000;
    for(auto h : hg) {
        for(auto trim : loaded_trims) {
            if(trim > h.minimum_trim) continue;
            float threshold_counts = std::get<0>(h.trim_map[trim]);
            hproj->Fill(threshold_counts);
            if(threshold_counts > max_x) max_x = threshold_counts;
            if(threshold_counts < min_x) min_x = threshold_counts;
        } // trim
    } // h

    // mostly most
    float x_value_max = hproj->GetBinCenter(hproj->GetMaximumBin());

    m_max_selected_threshold_cts = x_value_max;
    m_max_selected_threshold = x_to_mv(x_value_max);

    vector< tuple<int, int, int, int> > best_trims;
    for(auto h : hg) {
        float min_distance = 5000;
        int min_trim = 0;
        for(auto trim : loaded_trims) {
            if(trim > h.minimum_trim) continue;
            float threshold_counts = std::get<0>(h.trim_map[trim]);
            float distance = std::abs(threshold_counts - x_value_max);
            if(distance < min_distance) {
                min_distance = distance;
                min_trim = trim;
            }
        } // trim

        // values
        int board = h.board;
        int chip = h.chip;
        int channel = h.channel;
        int best_trim = min_trim;
        tuple<int, int, int, int> best_tuple(board, chip, channel, best_trim);
        best_trims.push_back(best_tuple);
    } // h

    // load
    for(auto b : loaded_boards) {
        for(auto c : loaded_chips) {
            vector< pair<int, int> > best_pairs;
            for(auto best : best_trims) {
                int check_board = std::get<0>(best);
                int check_chip = std::get<1>(best);
                if(!(check_board == b && check_chip == c)) continue;
                pair<int, int> best_pair(std::get<2>(best), std::get<3>(best));
                best_pairs.push_back(best_pair);
            } // best
            BoardChipPair bcpair(b,c);
            best_trims_global[bcpair] = best_pairs;
        } // c
    } // b
}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::make_summary_plots()
{
    // range per channel
    for(auto b : loaded_boards) {
        for(auto c : loaded_chips) {
            range_per_channel_plot(b, c);
            best_summary_plot_chip(b, c);
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::range_per_channel_plot(int board, int chip)
{
    gStyle->SetOptStat(0);

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip;

    stringstream n;
    n << "c_range_vs_channel" << coordstr.str();

    TCanvas* c0 = new TCanvas(n.str().c_str(), "", 1200, 600);
    c0->SetGrid(1,1);
    c0->cd();

    n.str("");
    n << "h_axis_range_trim" << coordstr.str();

    stringstream title;
    title << "Threshold Trimmer Range per Channel [board,chip]=[" << board << "," << chip << "]";
    TH1F* axis = new TH1F(n.str().c_str(), title.str().c_str(), 64, 0, 64);
    axis->GetXaxis()->SetTitle("VMM Channel");
    axis->GetXaxis()->SetTitleFont(42);
    axis->GetXaxis()->SetLabelFont(42);
    axis->GetYaxis()->SetTitle("Trimmer Range [steps]");
    axis->GetYaxis()->SetTitleFont(42);
    axis->GetYaxis()->SetLabelFont(42);

    vector<HolderChannelTrims> hc;
    for(auto x : holder) {
        int check_board = std::get<0>(x.first);
        int check_chip = std::get<1>(x.first);
        if(!(check_board == board && check_chip == chip)) continue;
        hc.push_back(x.second);
    }

    n << "_channel";
    TH1F* hrange0 = new TH1F(n.str().c_str(), title.str().c_str(), 64, 0, 64); 
    hrange0->SetFillColor(32);
    hrange0->SetLineColor(kBlack);

    hrange0->GetXaxis()->SetTitle("VMM Channel");
    hrange0->GetXaxis()->SetTitleFont(42);
    hrange0->GetXaxis()->SetLabelFont(42);
    hrange0->GetYaxis()->SetTitle("Trimmer Range [steps]");
    hrange0->GetYaxis()->SetTitleFont(42);
    hrange0->GetYaxis()->SetLabelFont(42);
    hrange0->SetFillColorAlpha(32, 0.5);

    int maxy = 40;

    n << "_mV";
    TH1F* hrange1 = new TH1F(n.str().c_str(), title.str().c_str(), 64, 0, 64); 
    hrange1->SetLineColor(kBlue);
    hrange1->SetLineWidth(1);

    float max_threshold_range_mv = -1;
    float min_threshold_range_mv = 5000;
    vector<float> thresh_values;
    for(auto ch : loaded_channels) {
        for(auto h : hc) {
            if(!(h.channel == ch)) continue;
            hrange0->SetBinContent(ch + 1, h.minimum_trim);
            if(h.threshold_range > max_threshold_range_mv) max_threshold_range_mv = h.threshold_range;
            if(h.threshold_range < min_threshold_range_mv) min_threshold_range_mv = h.threshold_range;
            thresh_values.push_back(h.threshold_range);
        } // h
    } // ch

    float rightmax = 1.1 * max_threshold_range_mv;
    for(int i = 0; i < (int)thresh_values.size(); i++) {
        hrange1->SetBinContent(i+1, thresh_values.at(i) * (maxy / rightmax) ); 
    }

    axis->Draw("axis");
    c0->Update();
    hrange0->SetMaximum(maxy);
    hrange0->Draw("hist");

    ////////////////////////////////////////
    // secondary axis with ranges in mV
    ////////////////////////////////////////

    float scale = gPad->GetUymax()/rightmax;
    hrange1->Draw("hist same");
    TGaxis* axis2 = new TGaxis(64, 0, 64, maxy, 0, rightmax, 510, "+L");
    axis2->SetTextSize(0.7*axis2->GetTextSize());
    axis2->SetLineColor(kBlue);
    axis2->SetLabelColor(kBlue);
    axis2->Draw();

    TLatex text;
    text.SetTextFont(42);
    text.SetTextColor(kBlue);
    text.SetTextAngle(90);
    text.SetTextSize(0.8*text.GetTextSize());
    text.DrawLatex(68, 23, "Trimmer Range [mV]");
    c0->Update();
    

    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "trimmer_range_vs_channel_" << coordstr.str();
    if(name() != "") n << "_" << name();
    if(m_subtract_baselines) n << "_BS";
    n << ".pdf";
    c0->SaveAs(n.str().c_str());

    delete c0;
    delete axis;
    delete hrange0;
    delete hrange1;
    delete axis2;

}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::best_summary_plot_chip(int board, int chip)
{

    vector<HolderChannelTrims> hch;

    for(auto x : holder) {
        int check_board = std::get<0>(x.first);
        int check_chip = std::get<1>(x.first);
        if(!(check_board == board && check_chip == chip))  continue;
        hch.push_back(x.second);
    }

    map<int, vector<int> > channels_for_trim;
    for(auto trim : loaded_trims) {
        for(auto chan : loaded_channels) {
            for(auto h : hch) {
                if(!(h.channel==chan)) continue;
                if(!(trim > h.minimum_trim)) {
                    channels_for_trim[trim].push_back(h.channel);
                }
            } // hch
        } // chan
    } // trim

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip;

    stringstream n;
    stringstream title;

    n << "h_axis_summary_chip" << coordstr.str();
    title << "Channel Threshold Ranges [board,chip]=["<<board<<","<<chip<<"]";

    int n_bins = loaded_channels.size();
    n_bins += 1;

    int x_low = loaded_channels.at(0);
    int x_high = loaded_channels.at(loaded_channels.size()-1);
    x_high += 1;

    TH1F* axis = new TH1F(n.str().c_str(), title.str().c_str(), n_bins, x_low, x_high);
    axis->GetXaxis()->SetTitle("VMM Channel");
    axis->GetXaxis()->SetTitleFont(42);
    axis->GetXaxis()->SetLabelFont(42);
    axis->GetYaxis()->SetTitle("Threshold [mV]");
    axis->GetYaxis()->SetTitleOffset(1.1*axis->GetYaxis()->GetTitleOffset());
    axis->GetYaxis()->SetTitleFont(42);
    axis->GetYaxis()->SetLabelFont(42);

    vector<TGraph*> graphs;

    float max_y = -1;
    float min_y = 5000;

    vector<int> bad_channels;
    for(auto trim : loaded_trims) {
        TGraph* g = new TGraph(loaded_channels.size());
        g->SetMarkerStyle(21);
        g->SetMarkerColor(kBlue);
        g->SetMarkerSize(0.75*g->GetMarkerSize());
        int ichan = 0;
        for(auto ch : loaded_channels) {
            for(auto h : hch) {
                if(!(h.channel == ch)) continue;
                if( trim > h.minimum_trim ) {
                    bad_channels.push_back(h.channel);
                    continue;
                }
                float thresh = std::get<0>(h.trim_map[trim]);
                thresh = x_to_mv(thresh);
                float x = (ch + 0.5);
                g->SetPoint(ichan, x, thresh);
                ichan++;

                if(thresh > max_y) max_y = thresh;
                if(thresh < min_y) min_y = thresh;
            } // h
        } // ch
        graphs.push_back(g);
    } // trim

    max_y = 1.1*max_y;
    min_y = 0.97*min_y;

    max_y = m_max_selected_threshold + 65;
    min_y = m_max_selected_threshold - 45;
    axis->GetYaxis()->SetRangeUser(min_y, max_y);

    ///////////////////////////////////////////////////
    // draw
    ///////////////////////////////////////////////////
    n.str("");
    n << "c_summary_chip" << coordstr.str();

    TCanvas* c0 = new TCanvas(n.str().c_str(), "", 800, 600);
    c0->SetGrid(1,1);
    c0->cd();

    axis->Draw("axis");
    c0->Update();

    bool is_first = true;
    for(auto g : graphs) {
        if(is_first) {
            g->Draw("p");
            is_first = false;
            c0->Update();
        }
        else {
            g->Draw("p");
            c0->Update();
        }
     } // g


    /// LINES
    BoardChipPair bcpair(board,chip);
    float avg_thresh_chip = 0.0;
    int n_total = 0;
    for(auto x : best_trims_chip[bcpair]) {
        int chan = std::get<0>(x);
        int best_trim = std::get<1>(x);
        BoardChipChannelTuple tuple(board,chip,chan);
        avg_thresh_chip += std::get<0>(holder[tuple].trim_map[best_trim]);
        n_total++;
    } // x

    float avg_thresh_board = 0.0;
    n_total = 0;
    for(auto x : best_trims_board[bcpair]) {
        int chan = std::get<0>(x);
        int best_trim = std::get<1>(x);
        BoardChipChannelTuple tuple(board,chip,chan);
        avg_thresh_board += std::get<0>(holder[tuple].trim_map[best_trim]);
        n_total++;
    } // x

    float avg_thresh_global = 0.0;
    n_total = 0;
    vector<std::pair<int, float>> global_chosens;
    for(auto x : best_trims_global[bcpair]) {
        int chan = std::get<0>(x);
        int best_trim = std::get<1>(x);
        BoardChipChannelTuple tuple(board,chip,chan);
        float thresh_here = std::get<0>(holder[tuple].trim_map[best_trim]);
        thresh_here = x_to_mv(thresh_here);
        pair<int, float> chosen(chan, thresh_here);
        global_chosens.push_back(chosen);
        avg_thresh_global += std::get<0>(holder[tuple].trim_map[best_trim]);
        n_total++;
    } // x

    avg_thresh_chip = (avg_thresh_chip / n_total);
    avg_thresh_chip = x_to_mv(avg_thresh_chip);


    TLine* line_chip = new TLine(x_low, avg_thresh_chip, x_high, avg_thresh_chip);
    line_chip->SetLineWidth(2);
    line_chip->SetLineColor(2);
    line_chip->SetLineStyle(2);

    avg_thresh_board = (avg_thresh_board / n_total);
    avg_thresh_board = x_to_mv(avg_thresh_board);

    TLine* line_board = new TLine(x_low, avg_thresh_board, x_high, avg_thresh_board);
    line_board->SetLineWidth(2);
    line_board->SetLineColor(4);
    line_board->SetLineStyle(8);

    avg_thresh_global = (avg_thresh_global / n_total);
    avg_thresh_global = x_to_mv(avg_thresh_global);

    TLine* line_global = new TLine(x_low, avg_thresh_global, x_high, avg_thresh_global);
    line_global->SetLineWidth(2);
    line_global->SetLineColor(3);
    line_global->SetLineStyle(1);


    line_global->Draw();
    c0->Update();
    line_board->Draw();
    c0->Update();
    line_chip->Draw();
    c0->Update();

    TLegend* leg = new TLegend(0.15, 0.70, 0.4, 0.88);
    leg->AddEntry(line_chip, "Chip", "l");
    leg->AddEntry(line_board, "Board", "l");
    stringstream gname;
    gname << "Global";// (#rightarrow " << std::setprecision(4) 
        //<< m_max_selected_threshold << " mV)";
    leg->AddEntry(line_global, gname.str().c_str(), "l");
    leg->SetBorderSize(0);
    leg->SetFillColor(0);

    // if we have loaded the threshol calibration, lets get it
    float set_threshold = calculate_threshold_dac(board, chip);

    float delta_set = -1.0;
    if(set_threshold > 0) {
        delta_set = (set_threshold - avg_thresh_global);

        TLine* line_set = new TLine(x_low, set_threshold, x_high, set_threshold);
        line_set->SetLineStyle(2);
        line_set->SetLineColor(kBlack);
        line_set->SetLineWidth(2);
        line_set->Draw();

        stringstream delta_set_name;
        delta_set_name << "VMM Set Threshold"; //(
        //    << std::setprecision(4) << set_threshold << " mV)";// (#Delta_{sel} = ";
        //delta_set_name << std::setprecision(4) << delta_set << " mV)";
        leg->AddEntry(line_set,delta_set_name.str().c_str(), "l");
    }

    TLine* line_max = new TLine(x_low, m_max_selected_threshold, x_high, m_max_selected_threshold);
    line_max->SetLineStyle(1);
    line_max->SetLineWidth(1);
    line_max->SetLineColor(kBlack);
    line_max->Draw();
    leg->AddEntry(line_max, "Max vote Threshold (global)", "l");

    TGraph* g_chosen = new TGraph(loaded_channels.size());
    stringstream chspread;
    chspread << "chosen_spread_" << board << "_" << chip;
    TH1F* chosen_spread = new TH1F(chspread.str().c_str(), "", 4096, 0, -1);
    int ichan = 0;
    for(auto ch : loaded_channels) {
        for(auto x : global_chosens) {
            if(!(std::get<0>(x)==ch)) continue;
            g_chosen->SetPoint(ichan, ch+0.7, std::get<1>(x));
            chosen_spread->Fill(std::get<1>(x));
            ichan++;
        } // x
    } // ch
    g_chosen->SetMarkerColor(kRed);
    g_chosen->SetMarkerSize(0.7*g_chosen->GetMarkerSize());
    g_chosen->SetMarkerStyle(20);

    chosen_spread->SaveAs("test.pdf");
    // chosen spread
    float chosen_avg = chosen_spread->GetMean();
    float chosen_rms = chosen_spread->GetRMS();

    g_chosen->Draw("p same");
    c0->Update();

    leg->AddEntry(g_chosen, "Chosen Channel Trimmer", "p");

    leg->Draw();
    c0->Update();

    TLatex text;
    title.str("");
    title << "Threshold Range Summary [board,chip]=["<<board<<","<<chip<<"]";
    text.SetTextFont(42);
    text.SetTextSize(0.83*text.GetTextSize());
    text.DrawLatex(0.15 * (x_high - x_low) + x_low, max_y + 1, title.str().c_str());

    title.str("");
    title << "Avg. threshold (selected) = " << std::setprecision(4) << chosen_avg << " #pm " << chosen_rms << " mV";
    text.SetTextSize(0.7*text.GetTextSize());
    text.DrawLatexNDC(0.45, 0.85, title.str().c_str());

    title.str("");
    title << "Max vote threshold (global) = " << std::setprecision(4) << m_max_selected_threshold << " mV"; 

    text.DrawLatexNDC(0.45, 0.80, title.str().c_str());

    title.str("");
    title << "VMM set threshold = " << std::setprecision(4) << set_threshold << " mV";
    text.DrawLatexNDC(0.45, 0.755, title.str().c_str());


    if(set_threshold > 0) {
        title.str("");
        title << "#Delta_{SET-MAX} = " << std::setprecision(4) << (set_threshold - m_max_selected_threshold) << " mV";
        text.DrawLatexNDC(0.45, 0.70, title.str().c_str());

        title.str("");
        title << "#Delta_{SET-#bar{SEL}} = " << std::setprecision(4) << delta_set << " mV"; 
        text.DrawLatexNDC(0.45, 0.65, title.str().c_str());
    }

    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "chip_summary_trims" << coordstr.str();
    if(name()!="") n << "_" << name();
    if(m_subtract_baselines) n << "_BS";
    n << ".pdf";
    c0->SaveAs(n.str().c_str());
    delete c0;
    
}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::write_txt_file()
{
    string type;
    type = "global";
    write_txt_file(type);

    type = "board";
    write_txt_file(type);

    type = "chip";
    write_txt_file(type);

}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::write_txt_file(string type)
{
    map<BoardChipPair, vector< pair<int, int> > > map;
    string t;
    if(type == "global") {
        map = best_trims_global;
        t = "GLOBAL";
    }
    else if(type == "board") {
        map = best_trims_board;
        t = "BOARD";
    } 
    else if(type == "chip") {
        map = best_trims_chip;
        t = "CHIP";
    }

    stringstream oname;

    oname << data_dir();
    char last = oname.str().back();
    if(last != '/') oname << "/";
    oname << "channel_trims_data_" << t;
    if(name()!="") oname << "_" << name();
    if(m_subtract_baselines) oname << "_BS";
    oname << ".txt";

    std::ofstream fg;
    fg.open(oname.str(), std::ofstream::out);

    //HEADER LINE
    std::string dsplit = "\t\t\t";
    fg << "#BOARD" << dsplit << "CHIP" << dsplit << "CHANNEL" << dsplit << "TRIM" << dsplit << "RANGE[mV]\n";
    for(auto board : loaded_boards) {
    for(auto chip : loaded_chips) {
        BoardChipPair bcpair(board, chip);
        vector<pair<int,int>> trim_pairs = map[bcpair];
    for(auto channel : loaded_channels) {
        BoardChipChannelTuple bctuple(board,chip,channel);
        float range = holder[bctuple].threshold_range;
    for(auto trim_pair : trim_pairs) {
        int check_channel = std::get<0>(trim_pair);
        if(!(check_channel==channel)) continue;
        int trim_to_set = std::get<1>(trim_pair);
        fg << board << dsplit << chip << dsplit << channel << dsplit << trim_to_set << dsplit << range << "\n";
    } // trim_pair
    } // channel
    } // chip
    } // board

    fg.close();

    cout << "vmmcalib    INFO Channel threshold trimmer calibration data (granularity=" << t <<")"
            << " stored in: " << oname.str() << endl;

}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::write_xml_file()
{
    string type;
    type = "global";
    write_xml_file(type);

    type = "board";
    write_xml_file(type);

    type = "chip";
    write_xml_file(type);
}
/////////////////////////////////////////////////////////////////////////////
void LooperChannelTrims::write_xml_file(string type)
{

    map<BoardChipPair, vector<pair<int, int> > > map;
    string t;
    if(type == "global") {
        map = best_trims_global;
        t = "GLOBAL";
    }
    else if(type == "board") {
        map = best_trims_board;
        t = "BOARD";
    }
    else if(type == "chip") {
        map = best_trims_chip;
        t = "CHIP";
    }

    using boost::property_tree::ptree;
    ptree pt;
    stringstream sx;

    ptree pt_board;
    for(auto board : loaded_boards) {
        pt_board.put("id", board);
        sx << "192.168.0." << board;
        pt_board.put("ip", sx.str());
        sx.str("");

        ptree pt_chip;
        for(auto chip : loaded_chips) {
            pt_chip.put("id", chip);

            BoardChipPair bcpair(board, chip);
            vector<pair<int,int>> trim_pairs = map[bcpair];

            ptree pt_channel;
            for(auto channel : loaded_channels) {
            for(auto trim_pair : trim_pairs) {
                int check_channel = std::get<0>(trim_pair);
                if(!(check_channel==channel)) continue;
                int trim_to_set = std::get<1>(trim_pair);

                sx.str("");
                sx << channel;
                pt_channel.put(sx.str(), trim_to_set);
            } // trim_pair
            } // channel
            pt_chip.add_child("channel_trim", pt_channel);
        } // chip
        pt_board.add_child("chip", pt_chip);
    } // board
    pt.add_child("board", pt_board);

    ptree final;
    final.add_child("threshold_calibration", pt);

    sx.str("");

    sx << data_dir();
    char last = sx.str().back();
    if(last != '/') sx << "/";
    sx << "channel_trims_data_" << t;
    if(name() != "") sx << "_" << name();
    if(m_subtract_baselines) sx << "_BS";
    sx << ".xml";

    #if BOOST_VERSION >= 105800
    boost::property_tree::write_xml(sx.str(), final, std::locale(),
        boost::property_tree::xml_writer_make_settings<std::string>('\t', 1));
    #else
    boost::property_tree::write_xml(sx.str(), final, std::local(),
        boost::property_tree::xml_writer_make_settings<char>('\t', 1));
    #endif
    
    cout << "vmmcalib    INFO Channel threshold trimmer calibration XML data (granularity=" << t <<")"
            << " stored in: " << sx.str() << endl;
}
