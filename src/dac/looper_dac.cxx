#include "looper_dac.h"

using namespace vmmcalib;

//std/stl
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
using namespace std;

//ROOT
#include "TStyle.h"
#include "TChain.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TProfile.h"
#include "TLatex.h"
#include "TF1.h"

/////////////////////////////////////////////////////////////////////////////
LooperDAC::LooperDAC()
{
    cout << " >>> LooperDAC" << endl;
}
/////////////////////////////////////////////////////////////////////////////
void LooperDAC::run_calibration()
{
    load_params();
    gStyle->SetOptStat(0);
    gStyle->SetOptFit(0);

    stringstream n;
    stringstream title;
    stringstream coordstr;

    string dac_type = "pulser";
    if(type()==vmmcalib::CalibrationType::ThresholdDACXADC) dac_type = "threshold";

    stringstream drawcmd;
    stringstream cutstr;

    int xlow = loaded_dacs.at(0);
    int xhigh = loaded_dacs.at(loaded_dacs.size()-1);

    holder.clear();
    for(auto board : loaded_boards) {
        for(auto chip : loaded_chips) {

            coordstr.str("");
            drawcmd.str("");

            coordstr << "_b" << board << "_c" << chip;

            n.str("");
            n << "h_" << dac_type << coordstr.str();

            drawcmd << "(samples/2^12)*1000:";
            if(dac_type=="pulser") drawcmd << "pulserCounts";
            else drawcmd << "dacCounts";

            drawcmd << ">>" << n.str();

            cutstr.str("");
            cutstr << "boardId=="<<board<<" && chip==" << chip;

            title.str("");
            if(dac_type=="pulser") title << "Pulser";
            else title << "Threshold";
            title << " DAC xADC Calibration [board,chip]=["<<board<<","<<chip<<"]";
            title << ";DAC [counts];xADC samples [mV]";


            // draw
            TH2F* h = new TH2F(n.str().c_str(), title.str().c_str(), 500, 0, 1000, 500, xlow, xhigh); 
            h->SetMarkerStyle(20);
            h->SetMarkerColor(kBlack);

            h->GetYaxis()->SetTitleFont(42);
            h->GetYaxis()->SetLabelFont(42);
            //h->GetYaxis()->SetTitleOffset(1.35*h->GetYaxis()->GetTitleOffset());
            h->GetXaxis()->SetTitleFont(42);
            h->GetXaxis()->SetLabelFont(42);

            chain()->Draw(drawcmd.str().c_str(), cutstr.str().c_str(), "goff");

            // convert to TProfile -- we don't need all of those points,
            // just the means
            n << "_axis";
            TProfile* prof = (TProfile*)h->ProfileX();

            n.str("");
            n << "c_" << dac_type << "_dac" << coordstr.str();
            TCanvas* c = new TCanvas(n.str().c_str(), "", 800, 600);
            c->SetGrid(1,1);
            c->cd();


            //h->Draw("p0");
            prof->Draw("p0 e");
            c->Update();

            TLatex text;
            text.SetTextAngle(90);
            text.SetTextFont(42);
            text.SetTextSize(0.7 * text.GetTextSize());
            text.DrawLatexNDC(0.035, 0.57, "xADC Samples [mV]");
            c->Update();

            n << "_fit";
            TF1* f = new TF1(n.str().c_str(), "pol1", xlow, xhigh);
            f->SetParNames("Constant", "Slope");
            prof->Fit(f, "QR");
            float constant = f->GetParameter(0);
            float constant_err = f->GetParError(0);
            float slope = f->GetParameter(1);
            float slope_err = f->GetParError(1);

            text.SetTextAngle(0);
            n.str("");
            n << "Slope      : " << std::setprecision(4) << slope << " #pm " << slope_err << " mV/cts";
            text.DrawLatexNDC(0.12, 0.85, n.str().c_str()); 
            n.str("");
            n << "Constant : " << std::setprecision(4) << constant << " #pm " << constant_err << " mV";
            text.DrawLatexNDC(0.12, 0.80, n.str().c_str());

            // save
            n.str("");
            n << plot_dir();
            char last = n.str().back();
            if(last != '/') n << "/";
            n << "dac_calib_" << dac_type << coordstr.str();
            if(name()!="") n << "_" << name();
            n << ".pdf";
            c->SaveAs(n.str().c_str());

            delete h;
            delete c;


            HolderDAC hd;
            hd.board = board;
            hd.chip = chip;
            hd.dac_slope = slope;
            hd.dac_slope_error = slope_err;
            hd.dac_constant = constant;
            hd.dac_constant_error = constant_err;

            BoardChipPair bcpair(board, chip);
            holder[bcpair] = hd;

        } // chip
    } // board

    write_txt_file();

}
/////////////////////////////////////////////////////////////////////////////
void LooperDAC::load_params()
{

    stringstream cutstr;
    stringstream drawcmd;

    ///////////////////////////////////////////////////////
    // get the board IDs run over
    ///////////////////////////////////////////////////////
    TH1F* board_histo = new TH1F("board_histo", "", 1000, 0, 1000);
    drawcmd.str("");
    drawcmd << "boardId>>board_histo";
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    int nbinsx = board_histo->GetNbinsX();
    loaded_boards.clear();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i+1;
        if(board_histo->GetBinContent(ibin) > 0) {
            int board = board_histo->GetBinLowEdge(ibin);
            if(m_select_board >= 0 && board != m_select_board) continue;
            loaded_boards.push_back(board);
        }
    }
    delete board_histo;
    std::sort(loaded_boards.begin(), loaded_boards.end());

    ///////////////////////////////////////////////////////
    // get the VMM #'s run over
    ///////////////////////////////////////////////////////
    TH1F* chip_histo = new TH1F("chip_histo", "", 8, 0, 8);
    drawcmd.str("");
    drawcmd << "chip>>chip_histo";
    chain()->Draw(drawcmd.str().c_str(), "", "goff");

    nbinsx = chip_histo->GetNbinsX();
    loaded_chips.clear();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i+1;
        if(chip_histo->GetBinContent(ibin) > 0) {
            int chip = chip_histo->GetBinLowEdge(ibin);
            if(m_select_chip >= 0 && chip!=m_select_chip) continue;
            loaded_chips.push_back(chip);
        }
    }
    std::sort(loaded_chips.begin(), loaded_chips.end());
    delete chip_histo;

    ///////////////////////////////////////////////////////
    // get the dac values run over
    ///////////////////////////////////////////////////////
    drawcmd.str("");
    TH1F* dac_histo = new TH1F("dac_histo", "", 1023, 0, 1023);
    bool is_pulser = true;
    if(type()==vmmcalib::CalibrationType::PulserDACXADC) drawcmd << "pulserCounts";
    else if(type()==vmmcalib::CalibrationType::ThresholdDACXADC) { is_pulser = false;  drawcmd << "dacCounts"; }
    drawcmd << ">>dac_histo";
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = dac_histo->GetNbinsX();
    loaded_dacs.clear();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i+1;
        if(dac_histo->GetBinContent(ibin) > 0) {
            int dac = dac_histo->GetBinLowEdge(ibin);
            if(is_pulser && m_select_pulser >= 0 && (dac!=m_select_pulser)) continue;
            loaded_dacs.push_back(dac);
        }
    }
    delete dac_histo;
    std::sort(loaded_dacs.begin(), loaded_dacs.end());


}
/////////////////////////////////////////////////////////////////////////////
void LooperDAC::write_txt_file()
{
    string dac_type = "pulser";
    if(type()==vmmcalib::CalibrationType::ThresholdDACXADC) dac_type = "threshold";
    stringstream n;
    n << data_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "dac_calib_" << dac_type << "_data";
    if(name()!="") n << "_" << name();
    n << ".txt";

    std::ofstream ofs;
    ofs.open(n.str(), std::ofstream::out);

    // HEADER
    std::string dsplit = "\t\t";
    ofs << "#BOARD" << dsplit << "CHIP" << dsplit << "DACSLOPE[MV/CTS]" << dsplit << "ERROR[MV/CTS]" << dsplit << "CONSTANT[mV]" << dsplit << "CONSTANTERROR[mV]\n";
    for(auto bch : holder) {
        BoardChipPair bcpair = bch.first;
        int board = std::get<0>(bcpair);
        int chip = std::get<1>(bcpair);
        float slope = bch.second.dac_slope;
        float error = bch.second.dac_slope_error;
        float constant = bch.second.dac_constant;
        float constant_err = bch.second.dac_constant_error;
        ofs << board << dsplit << chip << dsplit << std::setprecision(4)
                << slope << dsplit << error
                << dsplit << constant << dsplit << constant_err << "\n";
    } // bch

    ofs.close();

    cout << "vmmcalib    INFO DAC calib data stored in file: " << n.str() << endl;

}
