#include "looper_gains_pedestal.h"
using namespace vmmcalib;

//std/stl
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
using namespace std;

//ROOT
#include "TStyle.h"
#include "TLegend.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TChain.h"
#include "TF1.h"
#include "TLatex.h"


///////////////////////////////////////////////////////////////////////////////
LooperGainsPedestal::LooperGainsPedestal()
{
    cout << " >>> LooperGainsPedestal" << endl;
    gStyle->SetOptStat(0);
}
///////////////////////////////////////////////////////////////////////////////
void LooperGainsPedestal::run_calibration()
{
    //get_events(); // c.f. vmm_calibration_software/calib_base.h
    load_params();
    holder.clear();

    int n_total = (loaded_boards.size() * loaded_chips.size() * loaded_channels.size());
    float n_at = 0;

    for(auto board : loaded_boards) {
        for(auto chip : loaded_chips) {
            for(auto channel : loaded_channels) {
                cout << "vmmcalib    Determing gain and pedestal for [board,chip,channel]=["<<board<<","<<chip<<","<<channel<<"]"
                << " " << std::setw(6) << std::setprecision(2) << (n_at / n_total) * 100. << " \%" << endl;
                get_gain_and_pedestal(board,chip,channel);
                n_at++;
            } // channel
        } // chip
    } // board

    write_txt_file();

}
///////////////////////////////////////////////////////////////////////////////
void LooperGainsPedestal::load_params()
{
    cout << "vmmcalib    Loading calibration parameters" << endl;
    stringstream drawcmd;
    stringstream cutstr;

    //////////////////////////////////
    // get the board #'s
    //////////////////////////////////
    loaded_boards.clear();
    drawcmd.str("");
    cutstr.str("");
    drawcmd << "boardId>>board_histo";

    TH1F* board_histo = new TH1F("board_histo", "", 1000, 0, 1000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    int nbinsx = board_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        if(board_histo->GetBinContent(ibin) > 0) {
            int board = board_histo->GetBinLowEdge(ibin);
            if(m_select_board >= 0 && board != m_select_board) continue;
            loaded_boards.push_back(board);
        }
    }
    delete board_histo;
    std::sort(loaded_boards.begin(), loaded_boards.end());

    ////////////////////////////////////
    // get the chip #'s
    ////////////////////////////////////
    loaded_chips.clear();
    drawcmd.str("");
    cutstr.str("");
    drawcmd << "chip>>chip_histo";
    TH1F* chip_histo = new TH1F("chip_histo", "", 12, 0, 12);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = chip_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        if(chip_histo->GetBinContent(ibin) > 0) {
            int chip = chip_histo->GetBinLowEdge(ibin);
            if(m_select_chip >= 0 && chip != m_select_chip) continue;
            loaded_chips.push_back(chip);
        }
    }
    delete chip_histo;
    std::sort(loaded_chips.begin(), loaded_chips.end());

    ///////////////////////////////////////
    // get the channel #'s
    ///////////////////////////////////////
    loaded_channels.clear();
    drawcmd.str("");
    cutstr.str("");
    drawcmd << "channel>>channel_histo";
    TH1F* channel_histo = new TH1F("channel_histo", "", 100, 0, 100);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = channel_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        if(channel_histo->GetBinContent(ibin) > 0) {
            int channel = channel_histo->GetBinLowEdge(ibin);
            if(m_select_channel >= 0 && channel != m_select_channel) continue;
            loaded_channels.push_back(channel);
        }
    }
    delete channel_histo;
    std::sort(loaded_channels.begin(), loaded_channels.end());

    /////////////////////////////////////////
    // get the pulser DAC
    /////////////////////////////////////////
    loaded_dacs.clear();
    drawcmd.str("");
    cutstr.str("");
    drawcmd << "pulserCounts>>dac_histo";
    TH1F* dac_histo = new TH1F("dac_histo", "", 2000, 0, 2000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = dac_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        if(dac_histo->GetBinContent(ibin) > 0) {
            int dac = dac_histo->GetBinLowEdge(ibin);
            loaded_dacs.push_back(dac);
        }
    }
    delete dac_histo;
    std::sort(loaded_dacs.begin(), loaded_dacs.end());


    ///////////////////////////////////////////////////////////////////////////
    // summarize yo'self
    ///////////////////////////////////////////////////////////////////////////
    cout << "vmmcalib    Params loaded" << endl;
    cout << "  > loaded boards:    [";
    for(auto board : loaded_boards) cout << " " << board;
    cout << " ]" << endl;
    cout << "  > loaded chips:     [";
    for(auto chip : loaded_chips)  cout << " " << chip;
    cout << " ]" << endl;
    cout << "  > loaded channels:  [";
    for(auto chan : loaded_channels) cout << " " << chan;
    cout << " ]" << endl;
    int dac_step = loaded_dacs.at(1) - loaded_dacs.at(0);
    int dac_start = loaded_dacs.at(0);
    int dac_end = loaded_dacs.at(loaded_dacs.size()-1);
    cout << "  > loaded dac:       " << dac_start << " -> " << dac_end << ", step size = " << dac_step << endl;

}
///////////////////////////////////////////////////////////////////////////////
void LooperGainsPedestal::get_gain_and_pedestal(int board, int chip, int channel)
{
    BoardChipChannelTuple bcctuple(board, chip, channel);
    HolderGainsPedestal hgp;

    stringstream title;
    stringstream n;
    stringstream drawcmd;
    stringstream cutstr;

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip << "_ch" << channel;

    n.str("");
    n << "h_gain" << coordstr.str();
    title.str("");
    title << "Gain Curve [board,chip,channel]=["<<board<<","<<chip<<","<<channel<<"];Pulser DAC [cts];PDO [cts]";

    int xmin = loaded_dacs.at(0);
    int xhigh = loaded_dacs.at(loaded_dacs.size()-1);
    int ymin = 0.8 * xmin;
    int yhigh = 1.2 * xhigh;
    xmin = xmin - 10;
    xhigh = xhigh + 10;

    n << "c_" << n.str(); 
    TCanvas* c = new TCanvas(n.str().c_str(), "", 800, 600);
    c->SetGrid(1,1);
    c->cd();
    
    TH2F* h = new TH2F(n.str().c_str(), title.str().c_str(), 400, xmin, xhigh, 400, ymin, yhigh); 
    drawcmd.str("");
    drawcmd << "pdo:pulserCounts>>" << h->GetName();
    cutstr.str("");
    cutstr << "boardId=="<<board<<" && chip==" << chip <<" && channel==" << channel;
    chain()->Draw(drawcmd.str().c_str(), cutstr.str().c_str(), "goff");

    h->Draw();

    TProfile* prof = (TProfile*)h->ProfileX();
    prof->SetMarkerStyle(20);
    prof->SetMarkerSize(1.3 * prof->GetMarkerSize());
    prof->Draw("p0");
    c->Update();

    n << "fit_" << n.str();
    TF1* f = new TF1(n.str().c_str(), "pol1", xmin, xhigh);
    f->SetParNames("Pedestal", "Gain");
    prof->Fit(f, "QR");
    float pedestal = f->GetParameter(0);
    float pedestal_error = f->GetParError(0);
    float gain = f->GetParameter(1);
    float gain_error = f->GetParError(1);
    f->Draw("same");
    c->Update();

    TLatex text;
    text.SetTextFont(42);
    text.SetTextSize(0.7 * text.GetTextSize());
    n.str("");
    n << "Gain        : " << std::setprecision(4) << gain << " #pm " << gain_error;
    text.DrawLatexNDC(0.12, 0.85, n.str().c_str());
    c->Update();

    n.str("");
    n << "Pedestal  : " << std::setprecision(4) << pedestal << " #pm " << pedestal_error;
    text.DrawLatexNDC(0.12, 0.81, n.str().c_str());
    c->Update();

    // TProfile doesn't like y-axis labels?
    text.SetTextAngle(90);
    n.str("");
    text.DrawLatexNDC(0.035, 0.72, "PDO [cts]");
    c->Update();

    // save
    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "gain_curve" << coordstr.str();
    if(name()!="") n << "_" << name();
    n << ".pdf";
    c->SaveAs(n.str().c_str());

    // add to holer
    hgp.board = board;
    hgp.chip = chip;
    hgp.channel = channel;
    hgp.gain = gain;
    hgp.gain_error = gain_error;
    hgp.pedestal = pedestal;
    hgp.pedestal_error = pedestal_error;
    holder[bcctuple] = hgp;

}
///////////////////////////////////////////////////////////////////////////////
void LooperGainsPedestal::write_txt_file()
{

    stringstream n;
    n << data_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "gain_and_pedestal_data";
    if(name()!="") n << "_" << name();
    n << ".txt";

    std::ofstream ofs;
    ofs.open(n.str(), std::ofstream::out);

    // HEADER
    std::string ds = "\t";
    ofs << "#BOARD" << ds << "CHIP" << ds << "CHANNEL" << ds << "GAIN[CTS]" << ds << "GAINERR[CTS]" << ds << "PEDESTAL[CTS]" << ds << "PEDESTALERR[CTS]]\n";
    for(auto board : loaded_boards) {
        for(auto chip : loaded_chips) {
            for(auto chan : loaded_channels) {
                BoardChipChannelTuple tuple(board,chip,chan);
                HolderGainsPedestal hgp = holder[tuple];
                ofs << board << ds << chip << ds << chan << ds << hgp.gain << ds << hgp.gain_error << ds << hgp.pedestal << ds << hgp.pedestal_error << "\n";
            } // chan
        } // chip
    } // board

    ofs.close();
    cout << "vmmcalib    INFO Gain and pedestal calib data stored in file: " << n.str() << endl;



}
