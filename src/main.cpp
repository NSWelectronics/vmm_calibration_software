//std/stl
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

//calib_base
#include "calibration_type.h"
#include "looper_channel_trims.h"
#include "looper_baselines_noise.h"
#include "looper_dac.h"
#include "looper_gains_pedestal.h"
#include "looper_timing_ckbc_delay.h"

void help()
{
    cout << "------------------------------------------------------------------" << endl;
    cout << " vmmcalib" << endl;
    cout << endl;
    cout << " Usage: vmmcalib -i <input-calib-file> -t <type> [further options]" << endl;
    cout << endl;
    cout << " Options:" << endl;
    cout << "  -i                   input calibration ROOT ntuple [REQUIRED, POSITION 1]" << endl;
    cout << "  -t                   calibration type [REQUIRED, POSITION 2] (allowed types: " << vmmcalib::valid_types() << ")" << endl;
    cout << "  -n|--name            descriptive name to be appended to output files (default: \"\")" << endl;
    cout << "  -o|--output          output directory to place data and plots (default: ./)" << endl;
    cout << "  --board              select specific board number to calibrate (default: all found)" << endl;
    cout << "  --chip               select specific chip number to calibrate on all selected boards (default: all found)" << endl;
    cout << "  --channel            select specific channel number to calibrate on all selected boards and chips (default: all found)" << endl;
    cout << "  --plots              make all plots (by default, will only draw the 'summary' plots)" << endl;
    cout << "  --dbg                set debug level (integer)" << endl;
    cout << "  --select-gain        select a specific gain value (if you have run over multiple in the calibration loop)" << endl;
    cout << "  --select-peak-time   select a specific peak time value (if you have run over multiple in the calibration loop)" << endl;
    cout << "  --select-pulser      select a specific pulser DAC value (if you have run over multiple in the calibration loop)" << endl;
    cout << "  --select-threshold   select a specific threshold DAC value (if you have run over multiple in the calibration loop)" << endl;
    cout << "  --select-tac         select a specific TAC slope (if you have run over multiple in the calibration loop)" << endl;
    
    cout << "  -h|--help            print this help message" << endl;
    cout << endl;
    cout << " Calibration type specific options" << endl;
    cout << " Type: ChannelTrimsXADC" << endl;
    cout << "  --baselines-file     input text (*.txt) file used for subtracting baselines" << endl;
    cout << "  --threshold-file     input text (*.txt) file containing the threshold DAC calibration" << endl;
    cout << "  --n-chan-override    set the max number of events to load for each channel and channel trim (default: will load all events found)" << endl;
    cout << "------------------------------------------------------------------" << endl;


}

int main(int argc, char* argv[])
{

    string input_file = "";

    // required position arguments
    if(argc==1 || (argc==2 && ((string)argv[1] == "-h" || (string)argv[1] == "--help"))) {
        help();
        return 0;
    }
    else if(argc < 5 && argc > 0) {
        cout << "vmmcalib    ERROR You must provide at least two options (-i and -t are both required)" << endl;
        return 1;
    }

    // grab the input file
    if((string)argv[1] != "-i") {
        cout << "vmmcalib    ERROR First option provided must be '-i', providing the input calibration ntuple" << endl;
        return 1;
    }
    input_file = (string)argv[2];
    // check file exists
    bool file_found = std::ifstream(input_file).good();
    if(!file_found) {
        cout << "vmmcalib    ERROR Bad input file '" << input_file << "', exitting" << endl;
        return 1;
    }

    // grab the calibration type
    if ((string)argv[3] != "-t") {
        cout << "vmmcalib    ERROR Second option provided must be '-t', providing the calibration type" << endl;
        return 1;
    }
    string calib_type_name = (string)argv[4];

    if(!vmmcalib::is_valid_type(calib_type_name)) {
        cout << "vmmcalib    ERROR Provided calibration type '" << calib_type_name << "' is not a valid type (valid types = " << vmmcalib::valid_types() << ")" << endl;
        return 1;
    }
    vmmcalib::CalibrationType calib_type = vmmcalib::CalibrationTypeFromStr(calib_type_name);

    // the looper
    vmmcalib::CalibBase* looper;

    if(calib_type == vmmcalib::CalibrationType::ChannelTrimsXADC) looper = new vmmcalib::LooperChannelTrims();
    else if(calib_type == vmmcalib::CalibrationType::BaselinesXADC) looper = new vmmcalib::LooperBaselinesNoise();
    else if(calib_type == vmmcalib::CalibrationType::PulserDACXADC) looper = new vmmcalib::LooperDAC();
    else if(calib_type == vmmcalib::CalibrationType::ThresholdDACXADC) looper = new vmmcalib::LooperDAC(); 
    else if(calib_type == vmmcalib::CalibrationType::GainsPedestal) looper = new vmmcalib::LooperGainsPedestal();
    else if(calib_type == vmmcalib::CalibrationType::TimingCKBCDelay) looper = new vmmcalib::LooperTimingCKBC();
    else {
        cout << "vmmcalib    ERROR Provided calibration type is not yet supported for analysis!" << endl;
        return 1;
    }

    string name = "";
    string output_dir = "./";
    int select_board = -1;
    int select_chip = -1;
    int select_channel = -1;
    int dbg = 0;
    bool do_plots = false;
    int n_chan_override = -1;

    // specific selections
    float select_gain = -1.0;
    int select_peak_time = -1;
    int select_tac = -1;
    int select_threshold = -1;
    int select_pulser = -1;

    // options for specific calibration types
    // ChannelTrimsXADC
    string baselines_file = "";
    string threshold_file = "";

    int optin(1);
    while(optin < argc) {
        string in = argv[optin];
        
        if          (in == "-n" || in == "--name")      { name = argv[++optin]; }
        else if     (in == "-o" || in == "--output")    { output_dir = argv[++optin]; }
        else if     (in == "--board")                   { select_board = atoi(argv[++optin]); }
        else if     (in == "--chip")                    { select_chip = atoi(argv[++optin]); }
        else if     (in == "--channel")                 { select_channel = atoi(argv[++optin]); }
        else if     (in == "--plots")                   { do_plots = true; }
        else if     (in == "--dbg")                     { dbg = atoi(argv[++optin]); }
        else if     (in == "--select-gain")             { select_gain = atof(argv[++optin]); } 
        else if     (in == "--select-peak-time")        { select_peak_time = atoi(argv[++optin]); }
        else if     (in == "--select-tac")              { select_tac = atoi(argv[++optin]); }
        else if     (in == "--select-threshold")        { select_threshold = atoi(argv[++optin]); }
        else if     (in == "--select-pulser")           { select_pulser = atoi(argv[++optin]); }
        else if     (in == "-h" || in == "--help")      { help(); return 0; }
        else if     (in == "-t" || in == "-i")          { optin = optin+2; continue; }
        // -------------------------------------
        // ChannelTrimsXADC
        // -------------------------------------
        else if     (in == "--baselines-file")          { baselines_file = argv[++optin]; }
        else if     (in == "--threshold-file")          { threshold_file = argv[++optin]; }
        else if     (in == "--n-chan-override")         { n_chan_override = atoi(argv[++optin]); }
        else {
            cout << "vmmcalib    ERROR Unknown command line argument provided '" << in << "'" << endl;
            help();
            return 1;
        }
        optin++;
    }

    cout << "-----------------------------------------------------------------" << endl;
    cout << " vmmcalib" << endl;
    cout << endl;
    cout << " input file            : " << input_file << endl;
    cout << " calibration type      : " << vmmcalib::CalibrationType2Str(calib_type) << endl;
    cout << " name                  : " << name << endl;
    cout << " output directory      : " << output_dir << endl; 
    cout << " all plots             : " << do_plots << endl;
    cout << " selected board        : " << select_board << endl;
    cout << " selected chip         : " << select_chip << endl;
    cout << " selected channel      : " << select_channel << endl;
    cout << " debug level           : " << dbg << endl;
    cout << " selected gain         : " << select_gain << endl;
    cout << " selected peak time    : " << select_peak_time << endl;
    cout << " selected TAC slope    : " << select_tac << endl;
    cout << " selected threshold DAC: " << select_threshold << endl;
    cout << " selected pulser DAC   : " << select_pulser << endl;
    cout << " max events/{chan,trim}: " << n_chan_override << endl;
    cout << " baselines file        : " << baselines_file << endl;
    cout << " thresholds calib file : " << threshold_file << endl;
    cout << "-----------------------------------------------------------------" << endl;

    // propage the options
    looper->set_dbg(dbg);
    looper->set_name(name);
    looper->set_calibration_type(calib_type);
    looper->set_select_board(select_board);
    looper->set_select_chip(select_chip);
    looper->set_select_channel(select_channel);
    looper->set_select_gain(select_gain);
    looper->set_select_peak_time(select_peak_time);
    looper->set_select_tac(select_tac);
    looper->set_select_threshold(select_threshold);
    looper->set_select_pulser(select_pulser);
    looper->set_output_dir(output_dir);
    looper->set_all_plots(do_plots);
    looper->set_n_chan_override(n_chan_override);
    if(calib_type == vmmcalib::CalibrationType::ChannelTrimsXADC) {
        if(baselines_file !="") {
            file_found = std::ifstream(baselines_file).good();
            if(!file_found) {
                cout << "vmmcalib    ERROR Cannot open baselines file (=" << baselines_file << "), exiting" << endl;
                return 1;
            }
            if(!looper->load_baselines(baselines_file)) {
                cout << "vmmcalib    ERROR Unable to load baselines from file (=" << baselines_file << "), exiting" << endl;
                return 1;
            }
        }

        if(threshold_file != "") {
            file_found = std::ifstream(threshold_file).good();
            if(!file_found) {
                cout << "vmmcalib    ERROR Cannot open threshold calibration file (=" << threshold_file << "), exiting" << endl;
                return 1;
            }
            if(!looper->load_threshold_calibration(threshold_file)) {
                cout << "vmmcalib    ERROR Unable to load threshold DAC calibration from file (=" << threshold_file << "), exiting" << endl;
                return 1;
            }
        }
    }
    if(!looper->load_file(input_file)) {
        return 1;
    }
    looper->run_calibration();

    return 0;
}
