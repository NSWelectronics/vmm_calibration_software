#include "looper_timing_ckbc_delay.h"
using namespace vmmcalib;

//std/stl
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>
using namespace std;

//ROOT
#include "TStyle.h"
#include "TLegend.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TChain.h"
#include "TF1.h"
#include "TLatex.h"
#include "TGraphErrors.h"

///////////////////////////////////////////////////////////////////////////////
LooperTimingCKBC::LooperTimingCKBC()
{
    cout << " >>> LooperTimingCKBC" << endl;
    gStyle->SetOptStat(0);
}
///////////////////////////////////////////////////////////////////////////////
void LooperTimingCKBC::run_calibration()
{

    load_params();

    holder.clear();

    int n_total = (loaded_boards.size() * loaded_chips.size() * loaded_channels.size());
    float n_at = 0;

    for(auto board : loaded_boards) {
        for(auto chip : loaded_chips) {
            for(auto channel : loaded_channels) {
                cout << "vmmcalib    Determining timing constants and pedestals for [board,chip,channel]=["<<board<<","<<chip<<","<<channel<<"]"
                << " " << std::setw(6) << std::setprecision(2) << (n_at / n_total) * 100. << " \%" << endl;
                get_constants_and_pedestal(board,chip,channel);
                n_at++;
            } // channel
            summary_plots(board, chip);
        } // chip
    } // board

    write_txt_file();


}
///////////////////////////////////////////////////////////////////////////////
void LooperTimingCKBC::load_params()
{
    cout << "vmmcalib    Loading calibration parameters" << endl;
    stringstream drawcmd;
    stringstream cutstr;

    ///////////////////////////////////////////
    // get the board #'s
    ///////////////////////////////////////////
    loaded_boards.clear();
    drawcmd.str("");
    cutstr.str("");
    drawcmd << "boardId>>board_histo";

    TH1F* board_histo = new TH1F("board_histo", "", 1000, 0, 1000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    int nbinsx = board_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        int board = board_histo->GetBinLowEdge(ibin);
        if(board_histo->GetBinContent(ibin) > 0) {
            if(m_select_board >= 0 && board != m_select_board) continue;
            loaded_boards.push_back(board);
        }
    }
    delete board_histo;
    std::sort(loaded_boards.begin(), loaded_boards.end());

    ///////////////////////////////////////////////
    // get the chip #'s
    ///////////////////////////////////////////////
    loaded_chips.clear();
    drawcmd.str("");
    cutstr.str("");
    drawcmd << "chip>>chip_histo";
    TH1F* chip_histo = new TH1F("chip_histo", "", 12, 0, 12);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = chip_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        int chip = chip_histo->GetBinLowEdge(ibin);
        if(chip_histo->GetBinContent(ibin) > 0) {
            if(m_select_chip >= 0 && chip != m_select_chip) continue;
            loaded_chips.push_back(chip);
        }
    }
    delete chip_histo;
    std::sort(loaded_chips.begin(), loaded_chips.end());

    /////////////////////////////////////////////////////
    // get the channel #'s
    /////////////////////////////////////////////////////
    loaded_channels.clear();
    drawcmd.str("");
    cutstr.str("");
    drawcmd << "channel>>channel_histo";
    TH1F* channel_histo = new TH1F("channel_histo", "", 200, 0, 200);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = channel_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        int channel = channel_histo->GetBinLowEdge(ibin);
        if(channel_histo->GetBinContent(ibin) > 0) {
            if(m_select_channel >= 0 && channel != m_select_channel) continue; 
            loaded_channels.push_back(channel);
        }
    }
    delete channel_histo;
    std::sort(loaded_channels.begin(), loaded_channels.end());

    /////////////////////////////////////////////////////
    // bc latencies
    /////////////////////////////////////////////////////
    loaded_latencies.clear();
    drawcmd.str("");
    cutstr.str("");
    drawcmd << "bcLatency>>bc_histo";
    TH1F* bc_histo = new TH1F("bc_histo", "", 2000, 0, 2000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = bc_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        float bcl = bc_histo->GetBinLowEdge(ibin);
        if(bc_histo->GetBinContent(ibin) > 0) {
            loaded_latencies.push_back(bcl);
        }
    }
    delete bc_histo;
    std::sort(loaded_latencies.begin(), loaded_latencies.end());

    /////////////////////////////////////////////////////
    // check the TACs
    /////////////////////////////////////////////////////
    loaded_tacs.clear();
    drawcmd.str("");
    drawcmd << "tacSlope>>tac_histo";
    TH1F* tac_histo = new TH1F("tac_histo", "", 2000, 0, 2000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = tac_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        int tac = tac_histo->GetBinLowEdge(ibin);
        if(tac_histo->GetBinContent(ibin) > 0) {
            if(m_select_tac >= 0 && tac != m_select_tac) continue;
            loaded_tacs.push_back(tac);
        }
    }
    delete tac_histo;
    std::sort(loaded_tacs.begin(), loaded_tacs.end());
    if(loaded_tacs.size() > 1) {
        cout << "vmmcalib    ERROR Multiple TAC slopes encountered, only one expected. Exiting." << endl;
        exit(1);
    }

    /////////////////////////////////////////////////////
    // check the PTs
    /////////////////////////////////////////////////////
    loaded_pts.clear();
    drawcmd.str("");
    drawcmd << "peakTime>>pt_histo";
    TH1F* pt_histo = new TH1F("pt_histo", "", 2000, 0, 2000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = pt_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i + 1;
        int pt = pt_histo->GetBinLowEdge(ibin);
        if(pt_histo->GetBinContent(ibin) > 0) {
            if(m_select_peak_time >= 0 && pt != m_select_peak_time) continue;
            loaded_pts.push_back(pt);
        }
    }
    delete pt_histo;
    std::sort(loaded_pts.begin(), loaded_pts.end());
    if(loaded_pts.size() > 1) {
        cout << "vmmcalib    ERROR Multiple peak times encountered, only one expected. Exiting." << endl;
        exit(1);
    } 

    /////////////////////////////////////////////////////
    // check the gains
    /////////////////////////////////////////////////////
    vector<float> loaded_gains;
    drawcmd.str("");
    drawcmd << "gain>>g_histo";
    TH1F* g_histo = new TH1F("g_histo", "", 2000, 0, 2000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = g_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i+1;
        float g = g_histo->GetBinLowEdge(ibin);
        if(g_histo->GetBinContent(ibin) > 0) {
            if(m_select_gain >= 0 && g != m_select_gain) continue;
            loaded_gains.push_back(g);
        }
    }
    delete g_histo;
    std::sort(loaded_gains.begin(), loaded_gains.end());
    if(loaded_gains.size() > 1) {
        cout << "vmmcalib    ERROR Multiple gains encountered, only one expected. Exiting." << endl;
        exit(1);
    }

    //////////////////////////////////////////////////////////
    // check pulser/threshold
    //////////////////////////////////////////////////////////
    vector<int> dacs;
    drawcmd.str("");
    drawcmd << "pulserCounts>>dac_histo";
    TH1F* dac_histo = new TH1F("dac_histo", "", 2000, 0, 2000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = dac_histo->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i+1;
        int dac = dac_histo->GetBinLowEdge(ibin);
        if(dac_histo->GetBinContent(ibin) > 0) {
            if(m_select_pulser >= 0 && dac != m_select_pulser) continue;
            dacs.push_back(dac);
        }
    }
    delete dac_histo;
    std::sort(dacs.begin(), dacs.end());
    if(dacs.size() > 1) {
        cout << "vmmcalib    ERROR Multiple pulser DAC values encountered, only one expected. Exiting." << endl;
        exit(1);
    }
    dacs.clear();
    drawcmd.str("");
    drawcmd << "dacCounts>>dac_histo2";
    TH1F* dac_histo2 = new TH1F("dac_histo2", "", 2000, 0, 2000);
    chain()->Draw(drawcmd.str().c_str(), "", "goff");
    nbinsx = dac_histo2->GetNbinsX();
    for(int i = 0; i < nbinsx; i++) {
        int ibin = i+1;
        int dac = dac_histo2->GetBinLowEdge(ibin);
        if(dac_histo2->GetBinContent(ibin) > 0) {
            if(m_select_threshold >= 0 && dac != m_select_threshold) continue;
            dacs.push_back(dac);
        }
    }
    delete dac_histo2;
    std::sort(dacs.begin(), dacs.end());
    if(dacs.size() > 1) {
        cout << "vmmcalib    ERROR Multiple threshold DAC values encountered, only one expected. Exiting." << endl;
        exit(1);
    }

    //////////////////////////////////////////////////////////
    // summarize
    //////////////////////////////////////////////////////////
    cout << "vmmcalib    Params loaded" << endl;
    cout << " > loaded boards:    [";
    for(auto board : loaded_boards) cout << " " << board;
    cout << " ]" << endl;
    cout << " > loaded chips:     [";
    for(auto chip : loaded_chips) cout << " " << chip;
    cout << " ]" << endl;
    cout << " > loaded channels:  [";
    for(auto chan : loaded_channels) cout << " " << chan;
    cout << " ]" << endl;
    float latency_step = loaded_latencies.at(1) - loaded_latencies.at(0);
    float latency_start = loaded_latencies.at(0);
    float latency_end = loaded_latencies.at(loaded_latencies.size()-1);
    cout << " > loaded BC latencies : " << latency_start << " -> " << latency_end << ", step size = " << std::setprecision(4)  << latency_step << " ns" << endl;

}
///////////////////////////////////////////////////////////////////////////////
void LooperTimingCKBC::get_constants_and_pedestal(int board, int chip, int channel)
{

    BoardChipChannelTuple bcctuple(board, chip, channel);
    HolderCKBC hbc;

    stringstream title;
    stringstream n;
    stringstream drawcmd;
    stringstream cutstr;

    stringstream coordstr;
    coordstr << "_b" << board << "_c" << chip << "_ch" << channel;

    n.str("");
    n << "h_bc_delay" << coordstr.str();
    title.str("");
    title << "TDO vs BC latency [board,chip,channel]=["<<board<<","<<chip<<","<<channel<<"];BC Latency [ns]; TDO [cts]";

    int peak_time = loaded_pts.at(0);
    int tac_slope = loaded_tacs.at(0);

    int additive = 5;
    if(tac_slope > 100) additive = 15;
    
    float xmin_fit = peak_time + additive; // avoid the "flat" part due to the peaking time
    float xmin = 0;
    
    //float xmax_h = loaded_latencies.at(loaded_latencies.size()-1); // pad the ends
    float xmax_h = peak_time + 0.95 * tac_slope;
    float xmax_fit = 0.9 * xmax_h;
    int ymin = 0;
    int ymax = 256; // full TDO range possible

    n << "c_" << n.str();
    TCanvas* c = new TCanvas(n.str().c_str(), "", 800, 600);
    c->SetGrid(1,1);
    c->cd();

    TH2F* h = new TH2F(n.str().c_str(), title.str().c_str(), 400, 0, xmax_h, 256, ymin, ymax);
    drawcmd.str("");
    drawcmd << "tdo:bcLatency>>" << h->GetName();
    cutstr.str("");
    cutstr << "boardId=="<<board<<" && chip==" << chip << " && channel==" << channel << " && tacSlope==" << tac_slope << " && peakTime==" << peak_time;
    //cutstr << " && bcLatency > " << xmin << " && bcLatency < " << xmax;
    chain()->Draw(drawcmd.str().c_str(), cutstr.str().c_str(), "goff");

    h->Draw();

    TProfile* prof = (TProfile*)h->ProfileX();
    prof->SetMarkerStyle(20);
    prof->SetMarkerSize(1.1 * prof->GetMarkerSize());
    prof->Draw("p0");
    c->Update();

    n << "fit_" << n.str();
    TF1* f = new TF1(n.str().c_str(), "pol1", xmin_fit, xmax_fit);
    f->SetParNames("Pedestal Fit", "Slope");
    prof->Fit(f, "QR");
    float pedestal_fit = f->GetParameter(0);
    float pedestal_fit_error = f->GetParError(0);
    float slope = f->GetParameter(1);
    float slope_error = f->GetParError(1);
    f->Draw("same");
    c->Update();

    n << "fit2_ " << n.str();
    TF1* f2 = new TF1(n.str().c_str(), "pol0", 0, 0.9 * peak_time);
    f2->SetParNames("Pedestal Flat");
    prof->Fit(f2, "QR");
    float pedestal_flat = f2->GetParameter(0);
    float pedestal_flat_error = f2->GetParError(0);
    f2->SetLineColor(kBlue);
    f2->Draw("same");
    c->Update();

    TLatex text;
    text.SetTextFont(42);
    text.SetTextSize(0.7 * text.GetTextSize());
    n.str("");
    n << "Slope : " << std::setprecision(4) << slope << " #pm " << slope_error << " cts/ns";
    text.DrawLatexNDC(0.12, 0.85, n.str().c_str());
    c->Update();

    n.str("");
    n << "Pedestal from Fit : " << std::setprecision(4) << pedestal_fit << " #pm " << pedestal_fit_error << " cts";
    text.SetTextColor(kRed);
    text.DrawLatexNDC(0.12, 0.81, n.str().c_str());
    c->Update();

    n.str("");
    n << "Pedestal from Flat : " << std::setprecision(4) << pedestal_flat << " #pm " << pedestal_flat_error << " cts";
    text.SetTextColor(kBlue);
    text.DrawLatexNDC(0.12, 0.77, n.str().c_str());
    c->Update();

    text.SetTextColor(kBlack);

    // Y-axis?
    text.SetTextAngle(90);
    n.str("");
    text.DrawLatexNDC(0.035, 0.72, "TDO [cts]");
    c->Update();


    // save
    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "timing_calib_ckbc" << coordstr.str();
    if(name() != "") n << "_" << name();
    n << ".pdf";
    c->SaveAs(n.str().c_str());


    hbc.board = board;
    hbc.chip = chip;
    hbc.channel = channel;

    hbc.slope = slope;
    hbc.slope_error = slope_error;
    hbc.pedestal_fit = pedestal_fit;
    hbc.pedestal_fit_error = pedestal_fit_error;
    hbc.pedestal_flat = pedestal_flat;
    hbc.pedestal_flat_error = pedestal_flat_error;

    holder[bcctuple] = hbc;

}
///////////////////////////////////////////////////////////////////////////////
void LooperTimingCKBC::summary_plots(int board, int chip)
{

    stringstream n;
    stringstream title;
    stringstream coordstr;

    coordstr << "_b" << board << "_c" << chip;

    ///////////////////////////////////////////////////////////////
    // slope plots
    ///////////////////////////////////////////////////////////////

    n << "c_slopes" << coordstr.str();
    TCanvas* c = new TCanvas(n.str().c_str(), "", 800, 600);
    c->SetGrid(1,1);
    c->cd();

    TH1F* axis0 = new TH1F("axis0", "", loaded_channels.size(), loaded_channels.at(0), loaded_channels.at(loaded_channels.size()-1));
    axis0->GetXaxis()->SetTitleFont(42);
    axis0->GetXaxis()->SetLabelFont(42);
    axis0->GetXaxis()->SetTitle("VMM Channel");
    axis0->GetYaxis()->SetTitleFont(42);
    axis0->GetYaxis()->SetLabelFont(42);
    axis0->GetYaxis()->SetTitle("TDO Conversion Constant [cts/ns]");

    TGraphErrors* g0 = new TGraphErrors(loaded_channels.size());
    g0->SetMarkerStyle(20);
    g0->SetMarkerSize(1.1 * g0->GetMarkerSize());

    TGraphErrors* gfit = new TGraphErrors(0);
    gfit->SetMarkerStyle(20);
    gfit->SetMarkerColor(kRed);
    gfit->SetMarkerSize(1.1*gfit->GetMarkerSize());

    TGraphErrors* gflat = new TGraphErrors(0);
    gflat->SetMarkerStyle(20);
    gflat->SetMarkerColor(kBlue);
    gflat->SetMarkerSize(1.1*gflat->GetMarkerSize());

    float max_con = -1;
    float min_con = 10000;
    float max_ped = -1;
    float min_ped = 10000;

    for(auto data : holder) {
        BoardChipChannelTuple bctuple = data.first;
        int b = std::get<0>(bctuple);
        int c = std::get<1>(bctuple);
        int chan = std::get<2>(bctuple);
        if(!(b == board && c == chip)) continue;
        g0->SetPoint(g0->GetN(), chan, data.second.slope);
        g0->SetPointError(g0->GetN(), 0.0, data.second.slope_error);

        if(data.second.slope > max_con) max_con = data.second.slope;
        if(data.second.slope < min_con) min_con = data.second.slope;

        if(data.second.pedestal_fit > max_ped) max_ped = data.second.pedestal_fit;
        if(data.second.pedestal_fit < min_ped) min_ped = data.second.pedestal_fit;
        if(data.second.pedestal_flat > max_ped) max_ped = data.second.pedestal_flat;
        if(data.second.pedestal_flat < min_ped) min_ped = data.second.pedestal_flat;

        gfit->SetPoint(gfit->GetN(), chan, data.second.pedestal_fit);
        gfit->SetPointError(gfit->GetN(), 0.0, data.second.pedestal_fit_error);
        gflat->SetPoint(gflat->GetN(), chan, data.second.pedestal_flat);
        gflat->SetPointError(gflat->GetN(), chan, data.second.pedestal_flat_error);
    }
    axis0->SetMaximum(1.15*max_con);
    axis0->SetMinimum(0.95*min_con);
    axis0->Draw("axis");
    c->Update();
    g0->Draw("pe");
    c->Update();

    TLatex text;
    text.SetTextFont(42);
    n.str("");
    n << "Timing Constants for [board,chip]=["<<board<<","<<chip<<"]";
    text.DrawLatexNDC(0.2, 0.93, n.str().c_str());
    c->Update();

    // save
    n.str("");
    n << plot_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "timing_calib_ckbc_constants" << coordstr.str();
    if(name()!="") n << "_" << name();
    n << ".pdf";
    c->SaveAs(n.str().c_str());

    ///////////////////////////////////////////////////////////////
    // pedestal plots
    ///////////////////////////////////////////////////////////////

    n.str("");
    n << "c_pedestals" << coordstr.str();
    TCanvas* cp = new TCanvas(n.str().c_str(), "", 800, 600);
    cp->SetGrid(1,1);
    cp->cd();

    TH1F* ap = new TH1F("ap", "", loaded_channels.size(), loaded_channels.at(0), loaded_channels.at(loaded_channels.size()-1));
    ap->GetXaxis()->SetTitleFont(42);
    ap->GetXaxis()->SetLabelFont(42);
    ap->GetXaxis()->SetTitle("VMM Channel");
    ap->GetYaxis()->SetTitleFont(42);
    ap->GetYaxis()->SetLabelFont(42);
    ap->GetYaxis()->SetTitle("TDO Pedestal [cts]");
    ap->SetMaximum(1.15*max_ped);
    ap->SetMinimum(0.95*min_ped);
    ap->Draw("axis");
    cp->Update();

    gfit->Draw("pe");
    cp->Update();
    gflat->Draw("pe same");
    cp->Update();

    TLegend* leg = new TLegend(0.15, 0.75, 0.4, 0.88);
    leg->AddEntry(gfit, "Pedestal Fit", "p");
    leg->AddEntry(gflat, "Pedestal Flat", "p");
    leg->SetBorderSize(0);
    leg->SetFillColor(0);
    leg->Draw();
    cp->Update();

    n.str("");
    n << "TDO Pedestals for [board,chip]=["<<board<<","<<chip<<"]";
    text.DrawLatexNDC(0.2, 0.93, n.str().c_str());
    cp->Update();
    

    // save
    n.str("");
    n << plot_dir();
    if(last != '/') n << "/";
    n << "timing_calib_ckbc_pedestals" << coordstr.str();
    if(name()!="") n << "_" << name();
    n << ".pdf";
    cp->SaveAs(n.str().c_str());
    



    


}
///////////////////////////////////////////////////////////////////////////////
void LooperTimingCKBC::write_txt_file()
{
    stringstream n;
    n << data_dir();
    char last = n.str().back();
    if(last != '/') n << "/";
    n << "timing_calib_ckbc_data";
    if(name()!="") n << "_" << name();
    n << ".txt";

    std::ofstream ofs;
    ofs.open(n.str(), std::ofstream::out);

    // HEADER LINE
    string ds = "\t";
    ofs << "#BOARD"<<ds<<"CHIP"<<ds<<"CHANNEL"<<ds<<"CONST[cts/ns]"<<ds<<"ERR[cts/ns]"<<ds<<"PEDFIT[cts]"<<ds<<"ERR[cts]"<<ds<<"PEDFLAT[cts]"<<ds<<"ERR[cts]\n";
    for(auto board : loaded_boards) {
        for(auto chip : loaded_chips) {
            for(auto chan : loaded_channels) {
                BoardChipChannelTuple tuple(board,chip,chan);
                HolderCKBC hbc = holder[tuple];

                float cc = hbc.slope;
                float cc_error = hbc.slope_error;
                float ped_fit = hbc.pedestal_fit;
                float ped_fit_error = hbc.pedestal_fit_error;
                float ped_flat = hbc.pedestal_flat;
                float ped_flat_error = hbc.pedestal_flat_error;

                ofs << board << ds << chip << ds << chan << ds << cc << ds << cc_error
                    << ds << ped_fit << ds << ped_fit_error
                    << ds << ped_flat << ds << ped_flat_error << "\n";
            }
        } // chip
    } // board

    ofs.close();

    cout << "vmmcalib    INFO Timing CKBC delay calibration data stored in : " << n.str() << endl;

}
